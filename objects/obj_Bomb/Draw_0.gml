/// @description 
draw_self();
draw_set_alpha(0.5);
draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
draw_set_alpha(1);


if (point_in_rectangle(ROOM_MOUSE_X, ROOM_MOUSE_Y, bbox_left, bbox_top, bbox_right, bbox_bottom)) {
	type_formatted(x-1, y-21, "[fnt_Phone][fa_center][fa_bottom][c_white]Bomb");
	type_formatted(x, y-20, "[fnt_Phone][fa_center][fa_bottom][c_white]Bomb");	
}