/// @description 

occluder       = new BulbDynamicOccluder(obj_BulbRenderer.renderer);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _p1 = new Vec2(	-0.5*sprite_get_width(sprite_index), -0.5*sprite_get_height(sprite_index) );
var _p2 = new Vec2(	0.5*sprite_get_width(sprite_index), -0.5*sprite_get_height(sprite_index) );
var _p3 = new Vec2(	0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );
var _p4 = new Vec2(	-0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );


occluder.AddEdge( _p1.x, _p1.y, _p2.x, _p2.y );
occluder.AddEdge( _p2.x, _p2.y, _p3.x, _p3.y );
occluder.AddEdge( _p3.x, _p3.y, _p4.x, _p4.y );
occluder.AddEdge( _p4.x, _p4.y, _p1.x, _p1.y );