/// @description Signal booster change
//height_yellow = clamp(height_yellow + choose(1,-1)*irandom_range(5*SCALE, 10*SCALE), 20*SCALE, 30*SCALE);
//height_green = clamp(height_green + choose(1,-1)*irandom_range(5*SCALE, 10*SCALE), 10*SCALE, 20*SCALE);
alarm[4] = change_signal_booster_timer;
delta_red = choose(1,-1)*irandom_range(5*SCALE, 20*SCALE);
delta_yellow = choose(1,-1)*irandom_range(5*SCALE, 20*SCALE);
delta_green = choose(1,-1)*irandom_range(5*SCALE, 20*SCALE);