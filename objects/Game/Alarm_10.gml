/// @description Beep sound
if (room == room_Game_1) {
	var _dist = point_distance(obj_Player.x, obj_Player.y, obj_Bomb.x, obj_Bomb.y);
	var _max_dist = 260;
	if (option_value[? "Sounds"]) {
		sound_beep = audio_play_sound(snd_AlarmBeep, 10, false);
	}
	audio_sound_gain(sound_beep, 1-_dist/_max_dist, 0);	
	alarm[10] = 30;
}