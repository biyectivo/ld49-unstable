//  Handle fullscreen change
if (!BROWSER && keyboard_check(vk_lalt) && keyboard_check_pressed(vk_enter)) {
	option_value[? "Fullscreen"] = !option_value[? "Fullscreen"];
	fnc_SetGraphics();
}

// Browser "fullscreen" hack
if (BROWSER && keyboard_check_pressed(ord("F"))) {
	option_value[? "Fullscreen"] = !option_value[? "Fullscreen"];
	fnc_SetGraphics();
}
