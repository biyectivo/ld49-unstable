/// @description Bomb or fade explosion
total_time_taken = Game.alarm[3];
for (var _i=1; _i<=11; _i++) {
	Game.alarm[_i] = -1;
}
if (won) room_goto(room_UI_Won);
else if (lost) room_goto(room_UI_Lost);