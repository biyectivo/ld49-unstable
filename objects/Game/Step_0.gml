//if (ENABLE_LIVE && live_call()) return live_result;

if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (Game.lost && Game.paused && ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) {
	room_restart();
}

// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		
		// Register swipe/drag	
		device_mouse_dragswipe_register(0);
		
		// Global step
		if (instance_exists(obj_Player)) {
			current_step++;
		}		
		
		// Drag and swipe checks
		
		/*for (var _i=0; _i<1; _i++) {
			var _x = device_mouse_check_dragswipe_start(_i);
			var _y = device_mouse_check_dragswipe_end(_i);
			var _z = device_mouse_check_dragswipe(_i);
			if (_y) {
				var _d = device_mouse_dragswipe_distance(_i);
				var _t = device_mouse_dragswipe_time(_i);
				var _s = device_mouse_dragswipe_speed(_i);
			}
		}*/
		
		// Show phone
		if (keyboard_check_pressed(vk_tab)) {
			show_phone = !show_phone;
		}
		
		// Interact with bomb
		if (keyboard_check_pressed(vk_space) && point_distance(obj_Player.x, obj_Player.y, obj_Bomb.x, obj_Bomb.y)<3*TILE_SIZE) {
			show_bomb = !show_bomb;
		}
		
		// Interact with diagram
		if (keyboard_check_pressed(vk_space) && point_distance(obj_Player.x, obj_Player.y, obj_Diagram.x, obj_Diagram.y)<3*TILE_SIZE) {
			show_diagram = !show_diagram;
		}
		
		// Signal booster change logic
		if (alarm[4] > 0) {
			var _t = (change_signal_booster_timer-alarm[4])/change_signal_booster_timer;
			//show_debug_message(string(alarm[4])+" vs "+string(_t));
			height_red = fnc_Ease(height_red, clamp(height_red+ delta_red, 30, 50), _t, CSSTransitions, "ease-in-slow");	
			height_yellow = fnc_Ease(height_yellow, clamp(height_yellow+ delta_yellow, 5, 25), _t, CSSTransitions, "ease-in-slow");	
			height_green = fnc_Ease(height_green, clamp(height_green + delta_green, 8, 12), _t, CSSTransitions, "ease-in-slow");
			y_knob = clamp(y_knob, y1_signal_booster*SCALE, y1_signal_booster*SCALE + 2*height_red*SCALE+2*height_yellow*SCALE+height_green*SCALE);
		}
		
		// Control temperature (button mash)
		if (Game.current_step % 30 == 0) {
			bomb_temperature = clamp(bomb_temperature+0.15, 0, max_temperature);
			//show_debug_message(bomb_temperature);
		}
		
		if (show_bomb && keyboard_check_pressed(ord("Z")) || keyboard_check_pressed(ord("X"))) {			
			array_push(button_mash, keyboard_lastkey);
			array_resize_from_end(button_mash, 4);			
			array_push(button_mash_speed, get_timer());
			array_resize_from_end(button_mash_speed, 4);
			if (array_length(button_mash) == 4) {
				if (button_mash[0] == button_mash[2] && button_mash[1] == button_mash[3]) {
					var _delta = [button_mash_speed[1]-button_mash_speed[0], button_mash_speed[2]-button_mash_speed[1], button_mash_speed[3]-button_mash_speed[2]];
					var _max = max(_delta[0], _delta[1], _delta[2]);
					if (_max < 80000) {
						bomb_temperature = clamp(bomb_temperature-1, 0, max_temperature);
						//show_debug_message("Dropped 0.75");
					}
					else if (_max < 120000) {
						bomb_temperature = clamp(bomb_temperature-0.5, 0, max_temperature);
						//show_debug_message("Dropped 0.5");
					}
					else if (_max < 160000) {
						bomb_temperature = clamp(bomb_temperature-0.2, 0, max_temperature);
						//show_debug_message("Dropped 0.25");
					}
				}
			}
			
		}
		
		camera_shake = (bomb_temperature > max_bomb_temperature);
		
		// Signal booster controls
		if (keyboard_check(ord("Q"))) {
			y_knob = max(y_knob-SCALE, y1_signal_booster*SCALE);
		}
		if (keyboard_check(ord("A"))) {
			y_knob = min(y_knob+SCALE, y1_signal_booster*SCALE+2*height_red*SCALE+2*height_yellow*SCALE+height_green*SCALE);
		}
		
		
		if (option_value[? "Sounds"]) {
			if (!audio_is_playing(sound_heartbeat)) {
				sound_heartbeat = audio_play_sound(snd_Heartbeat,3,true);
			}
		}
		
		
		if (strikes == 3 || alarm[3] == 0 || (bomb_temperature > max_bomb_temperature && random(1)<0.0005)) {
			if (strikes == 3) lost_reason = "You had three strikes on the color pattern module.";
			else if (alarm[3] == 0) lost_reason = "You ran out of time.";
			else lost_reason = "The bomb became too hot and unstable and exploded.";
			lost = true;
		}
		
	}	
}


