
fnc_BackupDrawParams();

switch (room) {
	case room_UI_Intro:
		fnc_DrawIntro();
		break;
	case room_UI_Won:
		fnc_DrawYouWon();
		break;
	case room_UI_Lost:
		fnc_DrawYouLost();
		break;
	default:		
		if (Game.lost) {
			alarm[3] = -1;			
			audio_stop_sound(sound_beep);
			show_bomb = false;
			show_phone = false;
			show_diagram = false;
			if (alarm[11] == -1) {
				alarm[11] = 180;
			}
			if (option_value[? "Sounds"]) {
				if (!audio_is_playing(sound_explosion)) {
					sound_explosion  = audio_play_sound(snd_Explosion, 100, false);
					audio_sound_gain(sound_explosion,1,0);
				}
			}
			draw_set_alpha(1-alarm[11]/180);
			draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_white, c_white, c_white, c_white, false);
			draw_set_alpha(1);
		}
		else if (Game.won) {
			alarm[3] = -1;
			audio_stop_sound(sound_beep);
			show_bomb = false;
			show_phone = false;
			show_diagram = false;
			if (alarm[11] == -1) {
				alarm[11] = 180;
			}
			draw_set_alpha(1-alarm[11]/180);
			draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_black, c_black, c_black, c_black, false);
			draw_set_alpha(1);
		}
		else {
			fnc_DrawHUD();
		}
		break;
}

// Draw debug overlay on top of everything else
if (debug) {
	fnc_DrawDebug();
}

fnc_RestoreDrawParams();
