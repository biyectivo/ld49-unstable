// Enable views and set up graphics
view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	


if (room == room_Game_1) {
		
	// Define pathfinding grid
		
	grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
	// Add collision tiles to grid			
	for (var _col = 0; _col < room_width/GRID_RESOLUTION; _col++) {
		for (var _row = 0; _row < room_height/GRID_RESOLUTION; _row++) {				
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) != 0) {
				mp_grid_add_cell(grid, _col, _row);
			}
		}
	}
	
	
	// Initialize particle system and emitters
		
	particle_system = part_system_create_layer("lyr_Particles", true);
	particle_emitter_fire = part_emitter_create(particle_system);
		
	// Initialize player
	if (instance_exists(obj_Player)) {
		with (obj_Player) {
			event_perform(ev_other, ev_user0);
		}
	}
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Play music	
	if (option_value[? "Music"]) {
		music_sound_id = audio_play_sound(snd_Music, 5, true);
		audio_sound_gain(music_sound_id, 0.8, 0);
	}
	
	// Walls with occluders
	/*
	var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
	var _cols = tilemap_get_width(_tilemap);
	var _rows = tilemap_get_height(_tilemap);
	for (var _row=0; _row<_rows; _row++) {
		for (var _col=0; _col<_cols; _col++) {
			var _data = tilemap_get(_tilemap, _col, _row);
			var _ind = tile_get_index(_data);
			var _x = _col * TILE_SIZE + TILE_SIZE/2;
			var _y = _row * TILE_SIZE + TILE_SIZE/2;
			if (_ind == 1) { // Create wall object with occluder
				var _id = instance_create_layer(_x, _y, layer_get_id("lyr_Occluders"), obj_Wall);				
			}
		}
	}*/
	
	
	// Scribble - add fonts to included files first
	//scribble_font_add("fnt_Title");
		
}
