/// @description 
light = new BulbLight(obj_BulbRenderer.renderer, __sprite, 0, x, y);

with (light) {
	angle = other.__angle;
	blend = other.__color;
	xscale = other.__xscale;
	yscale = other.__yscale;
}