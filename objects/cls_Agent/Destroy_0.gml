/// @description Clean up the structures
if (ds_exists(state_speed, ds_type_map)) {
	ds_map_destroy(state_speed);	
}
if (ds_exists(state_machine, ds_type_map)) {
	ds_map_destroy(state_machine);
}