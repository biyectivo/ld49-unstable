/// @description Draw player
//if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {
	fnc_BackupDrawParams();
	
	// Shadow
	draw_set_alpha(0.7);
	draw_ellipse_color(x-8, y-6, x+8, y, $333333, $444444, false);
	draw_set_alpha(1);
	// Self
	draw_self();
	
	
	// Debug
	if (Game.debug) {
		type_formatted(x, y-10, "[fnt_Debug][fa_center][c_white]"+state+"/"+substate+"/"+string(image_index));
		draw_set_alpha(0.5);
		draw_rectangle_color(bbox_left, bbox_top, bbox_right, bbox_bottom, c_red, c_red, c_red, c_red, false);
		draw_set_alpha(1);
		
		if (horizontal_speed > 0) {
			draw_healthbar(x-sprite_width/2, y-sprite_height-15, x+sprite_width/2, y-sprite_height-10, horizontal_speed/max_horizontal_speed*100, c_black, c_green, c_lime, 0, true, true);
			//show_debug_message("s|>|"+string(Game.current_step)+"|"+string(horizontal_speed));
			//show_debug_message("x|>|"+string(Game.current_step)+"|"+string(x));
		}
		if (horizontal_speed < 0) {
			draw_healthbar(x-sprite_width/2, y-sprite_height-20, x+sprite_width/2, y-sprite_height-15, -horizontal_speed/max_horizontal_speed*100, c_black, c_red, c_fuchsia, 0, true, true);
			//show_debug_message("s|<|"+string(Game.current_step)+"|"+string(horizontal_speed));
			//show_debug_message("x|<|"+string(Game.current_step)+"|"+string(x));
		}
	}
	
	
	// Messages
	
	
	fnc_RestoreDrawParams();
}