/// @description Execute FSM
if (!Game.paused) {
	//if (ENABLE_LIVE && live_call()) return live_result;
	
	// Push previous state in queue and keep track of last substate
	if (last_state() != state) {
		array_push(state_history, state);			
		array_resize_from_end(state_history, max_state_history);		
	}
	last_substate = substate;	
	
	// Execute current FSM state
	state_machine[? state]();
	
	// Animation, Facing
	var _length = sprite_get_number(sprite_index)/animation_dirs;
	
	if (last_state() != state) {
		fnc_FSM_ResetStateAnimation();
	}
	else if (last_substate != substate) {
		fnc_FSM_ResetSubstateAnimation();
	}
	else {
		fnc_FSM_Animate();
	}
	
	// Active message	

	
	
	
	#region Lighting	
		/*
		occluder.x     = x;
		occluder.y     = y;
		occluder.angle = image_angle;
*/

		/*with (flashlight) {
			x = other.x+10;
			y = other.y;
			angle = other.image_angle;	
		}
		if (keyboard_check_pressed(ord("F"))) {
			flashlight.visible = !flashlight.visible;
		}*/
		
	#endregion
	
	
}



