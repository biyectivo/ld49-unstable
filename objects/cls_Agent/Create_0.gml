// Inventory
inventory = [];

// Struct
talk_node = function(_id, _message, _response_option_ids) constructor {
	id = _id;
	dialog_text = _message;
	response_id = _response_option_ids;
	
	toString = function() {
		return string(id)+": "+dialog_text+" "+array_print(response_id);
	}
}


//----------------------------------------------------------------------------------------------------
// INHERIT FROM HERE

/*

#region Stats
	name = "";
	max_hp = 1;
	hp = max_hp;
	
	player_speed = 4;	
#endregion


#region Other game-specific variables

#endregion

#region Finite State Machine
	state = "Idle";
	substate = "Down";	
	last_substate = "";
	state_history = [];
	max_state_history = 16;
	
	// Animation dirs
	animation_dirs = 4;	
	
	last_state = function() {
		return (array_length(state_history) > 0) ? state_history[array_length(state_history)-1] : "";
	}
	
	is_ai = true;
	controllable = false;
	horizontal_input = false;
	vertical_input = false;
	move_input = false;
	move_confirmed = false;
	move_angle = 0;
	path = noone;
	x_target = -1;
	y_target = -1;
	
	// State info
	state_speed = ds_map_create();
	ds_map_add(state_speed, "Idle", 10);
	ds_map_add(state_speed, "Move", 6);	
	
	// State functions
	state_machine = ds_map_create();
	
	ds_map_add(state_machine, "Idle", function() {
	
	});
	
	ds_map_add(state_machine, "Move", function() {

	});

	fnc_GetInputOrSensors = function() {	
		
	}

	fnc_PerformMove = function(_x, _y) {
		
	}


	fnc_Transition = function() {	
		
	}



	// Helper functions
	fnc_FSM_GetSubstateFromAngle = function(_move_angle) {
		var _substate;
		if (_move_angle >= 0 && _move_angle <= 45 || _move_angle > 315 && _move_angle < 360) _substate = "Right";
		else if (_move_angle > 45 && _move_angle <= 135) _substate = "Up";
		else if (_move_angle > 135 && _move_angle <= 225) _substate = "Left";
		else _substate = "Down";
	
		return _substate;
	}

	fnc_FSM_IndexOfSubstate = function(_substate) {
		var _index;
		switch (_substate) {
			case "Right":	_index = 0; break;
			case "Up":		_index = 1; break;
			case "Left":	_index = 2; break;
			case "Down":	_index = 3; break;
			default:		_index = 0; break;
		}
		return _index;
	}

	fnc_FSM_ResetStateAnimation = function() {
		var _obj = string_copy(object_get_name(object_index), 5, string_length(object_get_name(object_index))-4);
		sprite_index = asset_get_index("spr_"+_obj+"_"+state);
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Refresh length with new sprite
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;
	}

	fnc_FSM_ResetSubstateAnimation = function() {
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Calculate existing
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;		
	}

	fnc_FSM_Animate = function() {
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Calculate existing
		var _speed = state_speed[? state];
		if (Game.current_step % _speed == 0) {
			image_index = image_index + 1;
			var _index = fnc_FSM_IndexOfSubstate(substate);
			if (image_index == (_index+1)*_length) {
				image_index = _index * _length;
			}
		}
	}
	
	
#endregion



#region Dialog

	active_message = [];
	message_index = -1;
	
	// Multiple strings in the dialog text array can NOT be used when it's an option for the player
	dialog = [];
	dialog_actions = [];
	dialog_read = [];
	dialog_color = "[c_pink]";
	
	array_push(dialog,	new talk_node(cls_Agent, // 0
						function() { return [""]; },
						function() { return [1]; }));

	dialog_read = array_create(array_length(dialog), false);

	// Dialog actions

	dialog_actions = array_create(array_length(dialog), function() {});
	

#endregion


#region Lighting

// Lighting

occluder       = new BulbDynamicOccluder(obj_BulbRenderer.renderer);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _p1 = new Vec2(	-0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );
var _p2 = new Vec2(	-3,0.5*sprite_get_height(sprite_index)-2 );
var _p3 = new Vec2(	0, 0.5*sprite_get_height(sprite_index)-4 );
var _p4 = new Vec2(	3, 0.5*sprite_get_height(sprite_index)-2 );
var _p5 = new Vec2(	0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );

occluder.AddEdge( _p1.x, _p1.y, _p2.x, _p2.y );
occluder.AddEdge( _p2.x, _p2.y, _p3.x, _p3.y );
occluder.AddEdge( _p3.x, _p3.y, _p4.x, _p4.y );
occluder.AddEdge( _p4.x, _p4.y, _p5.x, _p5.y );
occluder.AddEdge( _p5.x, _p5.y, _p1.x, _p1.y );

//flashlight = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_6, 0, x+10, y);

with (flashlight) {
	angle = other.image_angle;
	blend = c_white;
	xscale = 1;
	yscale = 1;
	visible = false;
}


#endregion

fnc_FSM_ResetSubstateAnimation();

*/