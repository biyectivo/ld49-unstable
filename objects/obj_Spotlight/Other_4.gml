/// @description 
light = new BulbLight(obj_BulbRenderer.renderer, __sprite, 0, x, y);

flickering = random(1) < 0.6;

with (light) {
	angle = other.__angle;
	blend = other.__color;
	xscale = other.__xscale;
	yscale = other.__yscale;
}