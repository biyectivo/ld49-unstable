/// @description 
if (flickering) {
	if (random(1) < 0.9) {
		light.alpha = 0.2;	
	}
	else {
		light.alpha = 1;	
	}
}