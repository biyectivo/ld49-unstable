/// @description 
light = new BulbLight(obj_BulbRenderer.renderer, __sprite, 0, x, y);

normal_alpha = light.alpha;

flickering = random(1) < 0.25;

with (light) {
	angle = other.__angle;
	blend = other.__color;
	xscale = other.__xscale;
	yscale = other.__yscale;
}