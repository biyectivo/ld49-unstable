/// @description Create player


event_inherited();


#region Stats
	name = "";
	max_hp = 1;
	hp = max_hp;
	
	player_speed = 2;
	
	max_horizontal_speed = 4;
	horizontal_speed = 0;
	max_vertical_speed = 4;
	vertical_speed = 0;
	
	horizontal_acceleration = 0.04;
	horizontal_deceleration = 0.08;
	vertical_acceleration = 0.04;
	vertical_deceleration = 0.08;
#endregion


#region Other game-specific variables
	footstep_sound = noone;
#endregion



#region Finite State Machine
	state = "Idle";
	substate = "Down";	
	last_substate = "";
	state_history = [];
	max_state_history = 16;
	
	// Animation dirs
	animation_dirs = 4;	
	
	last_state = function() {
		return (array_length(state_history) > 0) ? state_history[array_length(state_history)-1] : "";
	}
	
	is_ai = false;
	controllable = true;
	horizontal_input = false;
	vertical_input = false;
	move_input = false;
	move_confirmed = false;
	move_angle = 0;
	path = noone;
	x_target = -1;
	y_target = -1;
	
	// State info
	state_speed = ds_map_create();
	ds_map_add(state_speed, "Idle", 6);
	ds_map_add(state_speed, "Move", 6);	
	ds_map_add(state_speed, "Die", 0);	
	
	// State functions
	state_machine = ds_map_create();
	
	ds_map_add(state_machine, "Idle", function() {
				
		// Get input
		fnc_GetInputOrSensors();
		
		// Transition to other states
		fnc_Transition();		
	});
	
	ds_map_add(state_machine, "Move", function() {

		if (Game.option_value[? "Sounds"]) {
			if (!audio_is_playing(footstep_sound)) {
				footstep_sound = audio_play_sound(choose(snd_Footstep1, snd_Footstep2, snd_Footstep3), 1, false);
			}
		}
		if (path_position == 0) {
			path_start(path, player_speed, path_action_stop, false);
		}
	
		if (path_position != 1) {
			move_angle = point_direction(x, y, x_target, y_target);
			substate = fnc_FSM_GetSubstateFromAngle(move_angle);
		}
	
		// Get input
		fnc_GetInputOrSensors();	
		// Transition to other states
		fnc_Transition();
		
	});
	
	ds_map_add(state_machine, "Die", function() {
		if (!is_ai) controllable = false;
	
		// Transition to other states
		fnc_Transition();		
	});

	fnc_GetInputOrSensors = function() {	
		if (is_ai) {
		}
		else {
			if (controllable) {
				move_input = device_mouse_check_button_pressed(0, mb_right);
				if (move_input) {			
					var _can_move = fnc_PerformMove(device_mouse_x(0), device_mouse_y(0));
				}
			}
		}
	}

	fnc_PerformMove = function(_x, _y) {
		x_target = _x;
		y_target = _y;
		var _id = instance_create_layer(_x, _y, "lyr_UI_Below", obj_Ripple);
		if (path_exists(path)) {			
			path_delete(path);
		}
		path = path_add();			
		move_confirmed = mp_grid_path(Game.grid, path, x, y, _x, _y, true);
		if (move_confirmed) {
			path_position = 0;			
		}
		return move_confirmed;
	}

	fnc_Transition = function() {	
		// Transition matrix	
		if (state == "Idle") {		
			if (move_input && move_confirmed) state = "Move";	
		}
		else if (state == "Move") {
			if (path_position == 1) {
				state = "Idle";
				move_angle = 0;
				move_confirmed = false;
				x_target = -1;
				y_target = -1;
				path_position = 0;
				if (path_exists(path)) {			
					path_delete(path);
				}
			}
		}
		else { // Die
		
		}
	}



	// Helper functions
	fnc_FSM_GetSubstateFromAngle = function(_move_angle) {
		var _substate;
		if (_move_angle >= 0 && _move_angle <= 45 || _move_angle > 315 && _move_angle < 360) _substate = "Right";
		else if (_move_angle > 45 && _move_angle <= 135) _substate = "Up";
		else if (_move_angle > 135 && _move_angle <= 225) _substate = "Left";
		else _substate = "Down";
	
		return _substate;
	}

	fnc_FSM_IndexOfSubstate = function(_substate) {
		var _index;
		switch (_substate) {
			case "Right":	_index = 0; break;
			case "Up":		_index = 1; break;
			case "Left":	_index = 2; break;
			case "Down":	_index = 3; break;
			default:		_index = 0; break;
		}
		return _index;
	}

	fnc_FSM_ResetStateAnimation = function() {
		var _obj = string_copy(object_get_name(object_index), 5, string_length(object_get_name(object_index))-4);
		sprite_index = asset_get_index("spr_"+_obj+"_"+state);
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Refresh length with new sprite
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;
		//show_debug_message("RSA l="+string(_length)+" i="+string(image_index)+" ss="+string(fnc_FSM_IndexOfSubstate(substate)));
	}

	fnc_FSM_ResetSubstateAnimation = function() {
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Calculate existing
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;
		//show_debug_message("RsA l="+string(_length)+" i="+string(image_index)+" ss="+string(fnc_FSM_IndexOfSubstate(substate)));
	}

	fnc_FSM_Animate = function() {
		var _length = sprite_get_number(sprite_index)/animation_dirs; // Calculate existing
		var _speed = state_speed[? state];
		if (Game.current_step % _speed == 0) {
			image_index = image_index + 1;
			var _index = fnc_FSM_IndexOfSubstate(substate);
			if (image_index == (_index+1)*_length) {
				image_index = _index * _length;
			}
		}
		//show_debug_message("ANI l="+string(_length)+" i="+string(image_index)+" ss="+string(fnc_FSM_IndexOfSubstate(substate)));
	}
	
	
#endregion



#region Dialog

	active_message = [];
	message_index = -1;
	
	// Multiple strings in the dialog text array can NOT be used when it's an option for the player
	dialog = [];
	dialog_actions = [];
	dialog_read = [];
	dialog_color = "[c_pink]";
	
	array_push(dialog,	new talk_node(cls_Agent, // 0
						function() { return [""]; },
						function() { return [1]; }));

	dialog_read = array_create(array_length(dialog), false);

	// Dialog actions

	dialog_actions = array_create(array_length(dialog), function() {});
	

#endregion


#region Lighting

// Lighting
/*
occluder       = new BulbDynamicOccluder(obj_BulbRenderer.renderer);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _p1 = new Vec2(	-0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );
var _p2 = new Vec2(	-3,0.5*sprite_get_height(sprite_index)-2 );
var _p3 = new Vec2(	0, 0.5*sprite_get_height(sprite_index)-4 );
var _p4 = new Vec2(	3, 0.5*sprite_get_height(sprite_index)-2 );
var _p5 = new Vec2(	0.5*sprite_get_width(sprite_index), 0.5*sprite_get_height(sprite_index) );

occluder.AddEdge( _p1.x, _p1.y, _p2.x, _p2.y );
occluder.AddEdge( _p2.x, _p2.y, _p3.x, _p3.y );
occluder.AddEdge( _p3.x, _p3.y, _p4.x, _p4.y );
occluder.AddEdge( _p4.x, _p4.y, _p5.x, _p5.y );
occluder.AddEdge( _p5.x, _p5.y, _p1.x, _p1.y );

//flashlight = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_6, 0, x+10, y);
/*
with (flashlight) {
	angle = other.image_angle;
	blend = c_white;
	xscale = 1;
	yscale = 1;
	visible = false;
}
*/

#endregion


fnc_FSM_ResetSubstateAnimation();