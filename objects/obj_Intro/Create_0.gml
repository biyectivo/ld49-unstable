/// @description 
slide = ["[$AAAAAA](headphones highly recommended)",

		"biyectivo presents",
		"a compo game for",
		spr_LudumDare,
		spr_Title,
		
		"The end of an era has arrived...",
		"an era where  genEnergy Inc.  has",
		"all but destroyed our most precious natural resources,",
		"ravaged our forests and contaminated our rivers.",
		"There is no coming back.",
		"You did not listen to our pleas.",
		"We are finally taking action.",
		"We are freeing the world from this atrocity,",
		"at whatever cost.",
		"The world will be unstable for a while,",
		"plutonium radiation will poison and kill",
		"your friends and family.",
		"And you, Mr. CEO, will be remembered",
		"as the one to blame for this tragedy.",
		"In [c_cyan]ten minutes[$DDDDDD], genEnergy Inc. and its plutonium reserves",
		"will be blown to bits,",
		"and the regeneration of our world will have begun.",
		"A new era will be born...",
		"[$333333].  .  .",
		
		"[c_white]OH GOD! Oh God oh God. 10 minutes!!!",
		"[c_white]My only hope is the Plant Director will reply to my urgent email,",
		"[c_white]that I sent seconds before being taken and locked in this bunker",
		"[c_white]with the bomb and the plutonium!!!.",		
		"[c_white]Move with [c_cyan]right mouse button",
		"[c_white]Interact with [c_cyan]SPACE",
		"[c_white]Use your smartphone with [c_cyan]TAB",
		"[c_white]Damn, I can't make any calls here... I need to [c_cyan]check my email...",
		" "
		];
		
		
special_speed = 160;
times = array_create(array_length(slide), special_speed); 
normal_speed = 150;
times[0] = 180;

times[1] = normal_speed;
times[2] = normal_speed;
times[3] = normal_speed;
times[4] = normal_speed;

times[10] = 140;
times[11] = 120;
times[14] = 120;
times[15] = 120;
times[16] = 120;

times[24] = 210;
times[25] = 210;
times[26] = 210;
times[27] = 210;
times[28] = 210;
times[29] = 210;
times[30] = 210;
times[31] = 210;


current = 0;

speech_sound = noone;
start_speech_idx = 5;

hb_sound = noone;

intro_shown = false;
tutorial_shown = false;