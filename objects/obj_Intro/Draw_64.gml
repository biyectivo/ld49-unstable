/// @description 
//if (ENABLE_LIVE && live_call()) return live_result;
if (current < array_length(slide)) {
	var _scale = SCALE;
	
	var _to_skip = (current < 24 ? "INTRO" : "TUTORIAL");
	type_formatted(GUI_WIDTH/2, GUI_HEIGHT-100, "[fnt_Phone][fa_center][fa_middle][$999999][scale/2,"+string(_scale/2)+"]PRESS SPACE TO SKIP "+_to_skip, true, true, 10);
	
	if (current == start_speech_idx && !audio_is_playing(speech_sound)) {
		speech_sound = audio_play_sound(snd_Speech, 10, false);
		audio_sound_gain(speech_sound,1,0);
	}
	
	if (is_string(slide[current])) {
		var _scale = SCALE;
		if (current >= 24) {
			var _speed = 1;
			var _scale_mod = 0.6;
		}
		else if (current >= 4) {
			var _speed = 2;
			var _scale_mod = 0.8;
		}
		else {
			var _scale_mod = 1;
			var _speed = 5;
		}
		type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2, "[fnt_Phone][fa_center][fa_middle][c_white][scale,"+string(_scale*_scale_mod)+"]"+slide[current], true, true, 0, 0, _speed);
	}
	else if (sprite_exists(slide[current])) {
		draw_sprite_ext(slide[current], 0, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE, SCALE, 0, c_white, 1);
	}
	if (alarm[0] == -1) alarm[0] = times[current];
}
else {
	instance_destroy();
	room_goto(room_Game_1);
}
