/// @description 
if (current >= 24) {
	intro_shown = true;
}

if (keyboard_check_pressed(vk_space)) {
	if (intro_shown) {
		room_goto(room_Game_1);
	}
	else {
		if (audio_is_playing(speech_sound)) audio_stop_sound(speech_sound);
		current = 24;
	}	
}

if (!audio_is_playing(hb_sound)) {
	hb_sound = audio_play_sound(snd_Heartbeat, 5, false);
	audio_sound_gain(hb_sound,1,0);
}