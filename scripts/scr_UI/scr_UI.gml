
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _y_title = 60;
		
		type_formatted(_w/2, _y_title, "[fnt_Title][fa_middle][fa_center][scale,1.2]"+_title_color+game_title);
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 100;
		var _spacing = 40;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center]"+_link_color+menu_items[_i], "[fa_middle][fa_center]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
		
		type_formatted(_w/2, _h-100, "[fnt_Title][fa_middle][fa_center][scale,0.8]A game by José Bonilla for [spr_LudumDare] 48");
			
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;

		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = GUI_MOUSE_X;
				var _mousey = GUI_MOUSE_Y;
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 60;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-30, _link_color+"[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}


function fnc_DrawIntro() {
	//if (ENABLE_LIVE && live_call()) return live_result;
}
function fnc_DrawYouWon() {
	//if (ENABLE_LIVE && live_call()) return live_result;
	var _x1 = GUI_WIDTH/2;
	var _y1 = GUI_HEIGHT/2-85*SCALE;
	var _scale = 0.7*SCALE;
	draw_sprite_ext(spr_Title, 0, _x1, _y1, SCALE/2, SCALE/2, 0, c_white, 1);
	draw_sprite_ext(spr_Bomb, 0, _x1, _y1+80*SCALE, SCALE/6, SCALE/6, 0, c_white, 1);
	
	var _total_seconds = floor(total_time_taken/60);
	var _minutes = floor(_total_seconds/60) < 10 ? "0"+string(floor(_total_seconds/60)) : string(_total_seconds/60);
	var _seconds = _total_seconds % 60 < 10 ? "0"+string(_total_seconds % 60) : string(_total_seconds % 60);
	
	var _t = type_formatted(_x1, _y1+180*SCALE, "[fnt_Phone][fa_center][fa_middle][scale,"+string(_scale)+"]You disarmed the bomb! "+string(_minutes)+" minutes and "+string(_seconds)+"seconds left on the bomb timer.",true, true, 0, 0, 5);
	if (_t.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {
		var _t2 = type_formatted(_x1, _y1+200*SCALE, "[fnt_Phone][fa_center][fa_middle][scale,"+string(_scale*0.8)+"]genEnergy Inc. lives!",true, true, 0, 0, 5);		
		if (_t2.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {		
			var _t3 = type_formatted(_x1, _y1+150*SCALE, "[fnt_Phone][fa_center][fa_middle][$DDDDDD][scale,"+string(_scale*0.8)+"]Press SPACE to try again...",true, true, 10);
		}
	}
	if (keyboard_check_pressed(vk_space)) {
		game_restart();
	}
}

function fnc_DrawYouLost() {
	//if (ENABLE_LIVE && live_call()) return live_result;
	var _x1 = GUI_WIDTH/2;
	var _y1 = GUI_HEIGHT/2-85*SCALE;
	var _scale = 0.7*SCALE;
	draw_sprite_ext(spr_Title, 0, _x1, _y1, SCALE/2, SCALE/2, 0, c_white, 1);
	draw_sprite_ext(spr_Bomb, 0, _x1, _y1+80*SCALE, SCALE/6, SCALE/6, 0, c_white, 1);
	var _t = type_formatted(_x1, _y1+180*SCALE, "[fnt_Phone][fa_center][fa_middle][scale,"+string(_scale)+"]You died.",true, true, 0, 0, 5);
	if (_t.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {
		var _t2 = type_formatted(_x1, _y1+200*SCALE, "[fnt_Phone][fa_center][fa_middle][scale,"+string(_scale)+"]"+lost_reason,true, true, 0, 0, 5);
		if (_t2.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {
			var _t3 = type_formatted(_x1, _y1+150*SCALE, "[fnt_Phone][fa_center][fa_middle][$DDDDDD][scale,"+string(_scale*0.8)+"]Press SPACE to try again...",true, true, 10);
		}
	}
	if (keyboard_check_pressed(vk_space)) {
		game_restart();
	}
}

function fnc_DrawHUD() {
	//if (ENABLE_LIVE && live_call()) return live_result;
	fnc_BackupDrawParams();
	// Draw the HUD
	
	// Diagram
	
	if (show_diagram) {
		var _x1 = GUI_WIDTH/2;	
		var _y1 = GUI_HEIGHT/2;	
		var _scale = 0.15*SCALE;
		draw_sprite_ext(spr_Diagram, 0, _x1, _y1, SCALE, SCALE, 0, c_white, 1);
		var _y1 = GUI_HEIGHT/2-40*SCALE;	
		type_formatted(_x1, _y1, "[fnt_Title][fa_center][fa_middle][$333333][scale,"+string(_scale)+"]HZ8374 Algorithm");
		var _y1 = GUI_HEIGHT/2-20*SCALE;	
		var _scale = 0.12*SCALE;
		type_formatted(_x1, _y1, "[fnt_Title][fa_center][fa_middle][$333333][scale,"+string(_scale)+"]Efficient multi-core");
		var _y1 = GUI_HEIGHT/2-10*SCALE;	
		type_formatted(_x1, _y1, "[fnt_Title][fa_center][fa_middle][$333333][scale,"+string(_scale)+"]clean energy gen");
		var _y1 = GUI_HEIGHT/2;	
		type_formatted(_x1, _y1, "[fnt_Title][fa_center][fa_middle][c_green][scale,"+string(_scale)+"]genEnergy Inc.");
		
		var _y1 = GUI_HEIGHT/2;
		
		var _cols = [c_blue, c_green, c_red, c_yellow];
		draw_set_alpha(0.5);	
		for (var _i=num_wires_to_cut-1; _i<num_wires_to_cut; _i++) {
			draw_rectangle_color(_x1-30*SCALE+_i*10*SCALE, _y1+20*SCALE, _x1-20*SCALE+_i*10*SCALE, _y1+40*SCALE, _cols[wires_to_cut[_i]], _cols[wires_to_cut[_i]], _cols[wires_to_cut[_i]], _cols[wires_to_cut[_i]], false);
		}
		var _x = _x1-30*SCALE+_i*10*SCALE;
		while (_x< _x1+30*SCALE) {			
			draw_rectangle_color(_x1-30*SCALE+_i*10*SCALE, _y1+20*SCALE, _x1-20*SCALE+_i*10*SCALE, _y1+40*SCALE, c_gray, c_gray, c_gray, c_gray, false);
			_i++;
			var _x = _x1-30*SCALE+_i*10*SCALE;
		}
		draw_set_alpha(1);
		
	}
		
	
	
	// Signal booster
	
	if (show_phone) {
		draw_set_alpha(0.8);
		var _x1_signal = GUI_WIDTH-80*SCALE;
		var _x2_signal = GUI_WIDTH-70*SCALE;
		var _y1_red = y1_signal_booster*SCALE;
		var _y2_red = _y1_red+height_red*SCALE;
		var _icon_signal_scale = 0.1*SCALE;
		type_formatted(GUI_WIDTH-22*SCALE, y1_signal_booster*SCALE-1*SCALE, "[fnt_Icons][fa_left][fa_bottom][c_gray][scale,"+string(_icon_signal_scale)+"]"+ICON_CELL_SIGNAL);
		draw_rectangle_color(_x1_signal, _y1_red, _x2_signal, _y2_red, $000066, $000066, c_red, c_red, false);
		if (point_in_rectangle(GUI_WIDTH-75*SCALE, y_knob, _x1_signal, _y1_red, _x2_signal, _y2_red)) {
			//show_debug_message("No signal");
			signal_status = SIGNAL.NO_SIGNAL;
		}
		var _y1_yellow = _y2_red;
		var _y2_yellow = _y1_yellow+height_yellow*SCALE;
		draw_rectangle_color(_x1_signal, _y1_yellow, _x2_signal, _y2_yellow, $00ccff, $00ccff, c_yellow, c_yellow, false);
		if (point_in_rectangle(GUI_WIDTH-75*SCALE, y_knob, _x1_signal, _y1_yellow, _x2_signal, _y2_yellow)) {
			//show_debug_message("Weak signal");
			signal_status = SIGNAL.WEAK_SIGNAL;
		}
		var _y1_green = _y2_yellow;
		var _y2_green = _y1_green+height_green*SCALE;
		draw_rectangle_color(_x1_signal, _y1_green, _x2_signal, _y2_green, $1a7438, $1a7438, c_green, c_green, false);
		if (point_in_rectangle(GUI_WIDTH-75*SCALE, y_knob, _x1_signal, _y1_green, _x2_signal, _y2_green)) {
			//show_debug_message("Fair signal");
			signal_status = SIGNAL.FAIR_SIGNAL;
		}
		var _y1_yellow = _y2_green;
		var _y2_yellow = _y1_yellow+height_yellow*SCALE;
		draw_rectangle_color(_x1_signal, _y1_yellow, _x2_signal, _y2_yellow, c_yellow, c_yellow, $00ccff, $00ccff, false);
		if (point_in_rectangle(GUI_WIDTH-75*SCALE, y_knob, _x1_signal, _y1_yellow, _x2_signal, _y2_yellow)) {
			//show_debug_message("Weak signal");
			signal_status = SIGNAL.WEAK_SIGNAL;
		}
		var _y1_red = _y2_yellow;
		var _y2_red = _y1_red+height_red*SCALE;
		draw_rectangle_color(_x1_signal, _y1_red, _x2_signal, _y2_red, c_red, c_red, $000066, $000066, false);
		if (point_in_rectangle(GUI_WIDTH-75*SCALE, y_knob, _x1_signal, _y1_red, _x2_signal, _y2_red)) {
			//show_debug_message("No signal");
			signal_status = SIGNAL.NO_SIGNAL;
		}
	
		draw_set_alpha(0.6);	
		draw_circle_color(GUI_WIDTH-75*SCALE, y_knob, 8*SCALE, c_white, $dddddd, false);
		draw_set_alpha(1);
	}
	
	
	
	if (show_bomb) {
		// Draw bomb
		draw_sprite_ext(spr_Bomb2, 0, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 1, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 2, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 3, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 4, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 5, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 6, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 7, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 8, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		draw_sprite_ext(spr_Bomb2, 9, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE*0.6, SCALE*0.6, 0, c_white, 1);
		
		
		var _x1 = GUI_WIDTH/2;
		var _y1 = 115*SCALE;
		var _scale_bomb_txt = 0.2*SCALE;
		var _strike_scale = 0.1*SCALE;
		var _total_seconds = floor(alarm[3]/60);
		var _minutes = floor(_total_seconds/60) < 10 ? "0"+string(floor(_total_seconds/60)) : string(_total_seconds/60);
		var _seconds = _total_seconds % 60 < 10 ? "0"+string(_total_seconds % 60) : string(_total_seconds % 60);
		if (display_strike) {
			type_formatted(_x1, _y1, "[fnt_BombLCD][c_red][fa_center][fa_bottom][scale,"+string(_strike_scale)+"]STRIKE "+string(strikes),true, true, 10);
		}
		else {
			type_formatted(_x1, _y1, "[fnt_BombLCD][c_red][fa_center][fa_middle][scale,"+string(_scale_bomb_txt)+"]"+_minutes+":"+_seconds,true, true, 10);
		}
		
		var _x1 = GUI_WIDTH/2+95*SCALE;
		var _y1 = GUI_HEIGHT/2+20*SCALE;
		var _laser_etch_scale = 0.05*SCALE;
		type_formatted(_x1, _y1, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_laser_etch_scale)+"]S/N: COMPO4LD49");
		draw_sprite_ext(spr_QR, 0, _x1, _y1+30*SCALE, SCALE*0.3, SCALE*0.3, 0, c_white, 1);
		
		// Four letter password
		var _x1 = GUI_WIDTH/2;
		var _y1 = GUI_HEIGHT/2 + 10*SCALE;
		var _mod_name_scale = 0.05*SCALE;
		draw_circle_color(_x1-27*SCALE, _y1, 2*SCALE, password_led_color, password_led_color, false);
		type_formatted(_x1, _y1, "[fnt_LaserEtch][c_black][fa_center][fa_middle][scale,"+string(_mod_name_scale)+"]MODULE #003");
		
		var _x1 = GUI_WIDTH/2-30*SCALE;
		var _y1 = GUI_HEIGHT/2+55*SCALE;
		var _password_scale = 0.1*SCALE;
		type_formatted(_x1, _y1, "[fnt_BombLCD][c_dkgray][fa_center][fa_middle][scale,"+string(_password_scale)+"]"+chr(four_letter_password_current[0]));
		var _x1 = GUI_WIDTH/2-10*SCALE;		
		type_formatted(_x1, _y1, "[fnt_BombLCD][c_dkgray][fa_center][fa_middle][scale,"+string(_password_scale)+"]"+chr(four_letter_password_current[1]));
		var _x1 = GUI_WIDTH/2+10*SCALE;		
		type_formatted(_x1, _y1, "[fnt_BombLCD][c_dkgray][fa_center][fa_middle][scale,"+string(_password_scale)+"]"+chr(four_letter_password_current[2]));
		var _x1 = GUI_WIDTH/2+30*SCALE;		
		type_formatted(_x1, _y1, "[fnt_BombLCD][c_dkgray][fa_center][fa_middle][scale,"+string(_password_scale)+"]"+chr(four_letter_password_current[3]));
		
		var _x1 = GUI_WIDTH/2-34*SCALE;
		var _y1 = GUI_HEIGHT/2+29*SCALE;
		var _x2 = _x1+10*SCALE;
		var _y2 = _y1;
		var _x3 = _x1+5*SCALE;
		var _y3 = _y1-10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[0] = (four_letter_password_current[0]-65+1) % 26 + 65;
			}
		}
		var _y1 = GUI_HEIGHT/2+71*SCALE;
		var _y2 = _y1;
		var _y3 = _y1+10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[0] = (four_letter_password_current[0]-65-1 == -1 ? 25 : four_letter_password_current[0]-65-1) % 26 + 65;
			}
		}
		
		var _x1 = GUI_WIDTH/2-15*SCALE;
		var _y1 = GUI_HEIGHT/2+29*SCALE;
		var _x2 = _x1+10*SCALE;
		var _y2 = _y1;
		var _x3 = _x1+5*SCALE;
		var _y3 = _y1-10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[1] = (four_letter_password_current[1]-65+1) % 26 + 65;
			}
		}
		var _y1 = GUI_HEIGHT/2+71*SCALE;
		var _y2 = _y1;
		var _y3 = _y1+10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[1] = (four_letter_password_current[1]-65-1 == -1 ? 25 : four_letter_password_current[1]-65-1) % 26 + 65;
			}
		}
		
		var _x1 = GUI_WIDTH/2+4*SCALE;
		var _y1 = GUI_HEIGHT/2+29*SCALE;
		var _x2 = _x1+10*SCALE;
		var _y2 = _y1;
		var _x3 = _x1+5*SCALE;
		var _y3 = _y1-10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[2] = (four_letter_password_current[2]-65+1) % 26 + 65;
			}
		}
		
		var _y1 = GUI_HEIGHT/2+71*SCALE;
		var _y2 = _y1;
		var _y3 = _y1+10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[2] = (four_letter_password_current[2]-65-1 == -1 ? 25 : four_letter_password_current[2]-65-1) % 26 + 65;
			}
		}
		
		var _x1 = GUI_WIDTH/2+24*SCALE;
		var _y1 = GUI_HEIGHT/2+29*SCALE;
		var _x2 = _x1+10*SCALE;
		var _y2 = _y1;
		var _x3 = _x1+5*SCALE;
		var _y3 = _y1-10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[3] = (four_letter_password_current[3]-65+1) % 26 + 65;
			}
		}
		
		var _y1 = GUI_HEIGHT/2+71*SCALE;
		var _y2 = _y1;
		var _y3 = _y1+10*SCALE;
		//draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, c_red, c_red, c_red, false);
		if (point_in_triangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2, _x3, _y3)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				four_letter_password_current[3] = (four_letter_password_current[3]-65-1 == -1 ? 25 : four_letter_password_current[3]-65-1) % 26 + 65;
			}
		}
		
		if (four_letter_password_current[0] == four_letter_password[0] && 
			four_letter_password_current[1] == four_letter_password[1] &&	
			four_letter_password_current[2] == four_letter_password[2] && 
			four_letter_password_current[3] == four_letter_password[3]) {
			//show_debug_message("Password: OK");
			password_led_color = c_lime;
		}
		else {
			password_led_color = c_red;
		}
		
		var _x1 = GUI_WIDTH/2+95*SCALE;
		var _x2 = _x1 + 2*SCALE;
		var _y1 = GUI_HEIGHT/2-78*SCALE;
		var _y2 = _y1+50*SCALE
		
		// Bomb Temperature
		draw_healthbar(_x1, _y1, _x2, _y2, bomb_temperature/max_temperature*100, c_black, c_blue, c_red, 3, false, false);
		var _num_scale = 0.05*SCALE;
		type_formatted(_x1+15*SCALE, _y2, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_num_scale)+"]0°");
		type_formatted(_x1+15*SCALE, _y2-12*SCALE, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_num_scale)+"]10°");
		type_formatted(_x1+15*SCALE, _y2-24*SCALE, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_num_scale)+"]20°");
		type_formatted(_x1+15*SCALE, _y2-36*SCALE, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_num_scale)+"]30°");
		type_formatted(_x1+15*SCALE, _y2-48*SCALE, "[fnt_LaserEtch][c_dkgray][fa_center][fa_middle][scale,"+string(_num_scale)+"]40°");
		
		// Simon says
		
		var _x1 = GUI_WIDTH/2-90*SCALE;
		var _y1 = GUI_HEIGHT/2 - 85*SCALE;
		var _mod_name_scale = 0.05*SCALE;		
		draw_circle_color(_x1-27*SCALE, _y1, 2*SCALE, simon_led_color, simon_led_color, false);
		type_formatted(_x1, _y1, "[fnt_LaserEtch][c_black][fa_center][fa_middle][scale,"+string(_mod_name_scale)+"]MODULE #001");
		
		
		var _can_press_buttons = (alarm[7] == -1 && alarm[8] == -1 && simon_led_color == c_red);
		
		
		if (simon_on && _can_press_buttons && array_length(current_simon) == current_simon_length) {
			//show_debug_message("Ya, presionaste:");
			array_print(current_simon);
			var _match = true;
			for (var _k=0; _k<current_simon_length; _k++) {
				_match = _match && current_simon[_k] == simon_says[_k];
			}
			if (_match) {
				current_simon_length++;
				if (current_simon_length <= array_length(simon_says)) {
					current_simon = [];
					current_simon_length_light_number = -1;
					alarm[7] = simon_speed;
				}
				else {
					//show_debug_message("Simon says: OK");	
					simon_led_color = c_lime;
				}
			}
			else {
				// reset simon
				simon_on = false;
				current_simon = [];
				current_simon_length = 0;
				current_simon_length_light_number = -1;
				currently_drawing_simon_light = false;
				alarm[7] = -1;
				alarm[8] = -1;
				// bomba
				fnc_Strike();
			}
		}
		
		var _x1 = GUI_WIDTH/2-124*SCALE;
		var _y1 = GUI_HEIGHT/2 - 77*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 0) draw_rectangle_color(_x1, _y1, _x2, _y2, c_blue, c_blue, c_blue, c_blue, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 0;
				alarm[5] = 15;
				array_push(current_simon, 0);
			}
		}
		var _x1 = GUI_WIDTH/2-105*SCALE;
		var _y1 = GUI_HEIGHT/2 - 77*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 1) draw_rectangle_color(_x1, _y1, _x2, _y2, c_red, c_red, c_red, c_red, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 1;
				alarm[5] = 15;
				array_push(current_simon, 1);
			}
		}
		var _x1 = GUI_WIDTH/2-86*SCALE;
		var _y1 = GUI_HEIGHT/2 - 77*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 2) draw_rectangle_color(_x1, _y1, _x2, _y2, c_lime, c_lime, c_lime, c_lime, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 2;
				alarm[5] = 15;
				array_push(current_simon, 2);
			}
		}
		var _x1 = GUI_WIDTH/2-124*SCALE;
		var _y1 = GUI_HEIGHT/2 - 57*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 3) draw_rectangle_color(_x1, _y1, _x2, _y2, c_yellow, c_yellow, c_yellow, c_yellow, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 3;
				alarm[5] = 15;
				array_push(current_simon, 3);
			}
		}
		var _x1 = GUI_WIDTH/2-105*SCALE;
		var _y1 = GUI_HEIGHT/2 - 57*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 4) draw_rectangle_color(_x1, _y1, _x2, _y2, c_fuchsia, c_fuchsia, c_fuchsia, c_fuchsia, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 4;
				alarm[5] = 15;
				array_push(current_simon, 4);
			}
		}
		var _x1 = GUI_WIDTH/2-86*SCALE;
		var _y1 = GUI_HEIGHT/2 - 57*SCALE;
		var _x2 = _x1 + 18*SCALE;		
		var _y2 = _y1 + 18*SCALE;
		if (simon_light == 5) draw_rectangle_color(_x1, _y1, _x2, _y2, c_aqua, c_aqua, c_aqua, c_aqua, false);
		if (_can_press_buttons && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				simon_light = 5;
				alarm[5] = 15;
				array_push(current_simon, 5);
			}
		}
		
		//on-off
		
		if (simon_on) {
			var _x1 = GUI_WIDTH/2-88*SCALE;
			var _y1 = GUI_HEIGHT/2 - 28*SCALE;
			var _x2 = _x1 + 3*SCALE;		
			var _y2 = _y1 + 8*SCALE;
			draw_set_alpha(0.8);
			draw_rectangle_color(_x1, _y1, _x2, _y2, c_lime, c_lime, c_lime, c_lime, false);
			draw_set_alpha(1);
		}
		else {
			var _x1 = GUI_WIDTH/2-78*SCALE;
			var _y1 = GUI_HEIGHT/2 - 28*SCALE;
			var _x2 = _x1 + 3*SCALE;		
			var _y2 = _y1 + 8*SCALE;
			draw_set_alpha(0.8);
			draw_rectangle_color(_x1, _y1, _x2, _y2, c_red, c_red, c_red, c_red, false);
			draw_set_alpha(1);
		}
		
		var _x1 = GUI_WIDTH/2-88*SCALE;
		var _y1 = GUI_HEIGHT/2 - 28*SCALE;
		var _x2 = _x1 + 13*SCALE;		
		var _y2 = _y1 + 8*SCALE;
		if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (simon_led_color == c_red && device_mouse_check_button_pressed(0, mb_left)) {
				simon_on = !simon_on;
				if (!simon_on) {
					// reset simon
					current_simon = [];
					current_simon_length = 0;
					current_simon_length_light_number = -1;
					currently_drawing_simon_light = false;
					alarm[7] = -1;
					alarm[8] = -1;
				}
				else { // Start with length 1
					current_simon_length = 1;
					alarm[7] = simon_speed;			
				}
			}
		}
		if (currently_drawing_simon_light) {
			draw_set_alpha(0.8);
			draw_circle_color(GUI_WIDTH/2-111*SCALE, GUI_HEIGHT/2-24*SCALE, 10*SCALE, simon_colors[simon_says[current_simon_length_light_number]], simon_colors[simon_says[current_simon_length_light_number]], false);
			draw_set_alpha(1);
		}
		
		// Wires
		var _x1 = GUI_WIDTH/2-90*SCALE;
		var _y1 = GUI_HEIGHT/2 + 10*SCALE;
		var _mod_name_scale = 0.05*SCALE;
		draw_circle_color(_x1-27*SCALE, _y1, 2*SCALE, wires_led_color, wires_led_color, false);
		type_formatted(_x1, _y1, "[fnt_LaserEtch][c_black][fa_center][fa_middle][scale,"+string(_mod_name_scale)+"]MODULE #002");
		
		var _cut_color = $e4eadf;
		
		if (array_length(actual_wires_cut) > 0) {
			if (array_length(actual_wires_cut) == array_length(wires_to_cut)) {
				var _cut_correctly = true;
				for (var _j=0; _j<array_length(actual_wires_cut); _j++) {
					_cut_correctly = _cut_correctly && actual_wires_cut[_j] == wires_to_cut[_j];
				}
				if (_cut_correctly) {
					//show_debug_message("Wires: OK");	
					wires_led_color = c_lime;
				}
				else {
					wires_led_color = c_red;
					lost_reason = "You cut the wires in the wrong order.";
					lost = true;
					// TODO
				}
			}
		}
		
		
		//blue
		
		var _blue_wire_cut = (array_find(actual_wires_cut, WIRES.BLUE) != -1);
		if (_blue_wire_cut) {
			var _x1 = GUI_WIDTH/2-120*SCALE;
			var _y1 = GUI_HEIGHT/2+25*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
			var _x1 = GUI_WIDTH/2-114*SCALE;
			var _y1 = GUI_HEIGHT/2+25*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
		}
		
		//green
		var _green_wire_cut = (array_find(actual_wires_cut, WIRES.GREEN) != -1);
		if (_green_wire_cut) {
			var _x1 = GUI_WIDTH/2-90*SCALE;
			var _y1 = GUI_HEIGHT/2+25*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
			var _x1 = GUI_WIDTH/2-90*SCALE;
			var _y1 = GUI_HEIGHT/2+22*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
		}
		
		//red
		var _red_wire_cut = (array_find(actual_wires_cut, WIRES.RED) != -1);
		if (_red_wire_cut) {
			var _x1 = GUI_WIDTH/2-80*SCALE;
			var _y1 = GUI_HEIGHT/2+50*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
			var _x1 = GUI_WIDTH/2-78*SCALE;
			var _y1 = GUI_HEIGHT/2+53*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
		}
		
		//yellow
		var _yellow_wire_cut = (array_find(actual_wires_cut, WIRES.YELLOW) != -1);
		if (_yellow_wire_cut) {
			var _x1 = GUI_WIDTH/2-110*SCALE;
			var _y1 = GUI_HEIGHT/2+68*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
			var _x1 = GUI_WIDTH/2-104*SCALE;
			var _y1 = GUI_HEIGHT/2+69*SCALE;
			var _x2 = _x1 + 20*SCALE;
			var _y2 = _y1;
			var _x3 = _x1 + 5*SCALE;
			var _y3 = _y1+4*SCALE;
			draw_triangle_color(_x1, _y1, _x2, _y2, _x3, _y3, _cut_color,_cut_color, _cut_color, false);
		}
		
		var _x1 = GUI_WIDTH/2-130*SCALE;
		var _y1 = GUI_HEIGHT/2+20*SCALE;
		var _x2 = _x1 + 40*SCALE;
		var _y2 = _y1 + 15*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (!_blue_wire_cut && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				array_push(actual_wires_cut, WIRES.BLUE);
			}
		}
		
		var _x1 = GUI_WIDTH/2-86*SCALE;
		var _y1 = GUI_HEIGHT/2+10*SCALE;
		var _x2 = _x1 + 12*SCALE;
		var _y2 = _y1 + 30*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (!_green_wire_cut && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				array_push(actual_wires_cut, WIRES.GREEN);
			}
		}
		
		var _x1 = GUI_WIDTH/2-84*SCALE;
		var _y1 = GUI_HEIGHT/2+46*SCALE;
		var _x2 = _x1 + 16*SCALE;
		var _y2 = _y1 + 30*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (!_red_wire_cut && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				array_push(actual_wires_cut, WIRES.RED);
			}
		}
		
		
		var _x1 = GUI_WIDTH/2-125*SCALE;
		var _y1 = GUI_HEIGHT/2+60*SCALE;
		var _x2 = _x1 + 35*SCALE;
		var _y2 = _y1 + 15*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		
		if (!_yellow_wire_cut && point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (device_mouse_check_button_pressed(0, mb_left)) {
				array_push(actual_wires_cut, WIRES.YELLOW);
			}
		}
		
		// Disarm sequence
		
		var _x1 = GUI_WIDTH/2-26*SCALE;
		var _y1 = GUI_HEIGHT/2-29*SCALE;
		var _x2 = _x1 + 13*SCALE;
		var _y2 = _y1 + 18*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (simon_led_color == c_lime && password_led_color == c_lime && wires_led_color == c_lime && device_mouse_check_button_pressed(0, mb_left)) {
				if (option_value[? "Sounds"]) {
					audio_play_sound(snd_Cut_Click, 5, false);
				}
				array_push(current_module_order, 0);
			}
		}
		type_formatted(GUI_WIDTH/2-19*SCALE, GUI_HEIGHT/2-20*SCALE, "[c_orange][fa_middle][fa_center]1");
		
		var _x1 = GUI_WIDTH/2-6*SCALE;
		var _y1 = GUI_HEIGHT/2-29*SCALE;
		var _x2 = _x1 + 13*SCALE;
		var _y2 = _y1 + 18*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (simon_led_color == c_lime && password_led_color == c_lime && wires_led_color == c_lime && device_mouse_check_button_pressed(0, mb_left)) {
				if (option_value[? "Sounds"]) {
					audio_play_sound(snd_Cut_Click, 5, false);
				}
				array_push(current_module_order, 1);
			}
		}
		type_formatted(GUI_WIDTH/2+1*SCALE, GUI_HEIGHT/2-20*SCALE, "[c_orange][fa_middle][fa_center]2");
		
		
		var _x1 = GUI_WIDTH/2+12*SCALE;
		var _y1 = GUI_HEIGHT/2-29*SCALE;
		var _x2 = _x1 + 13*SCALE;
		var _y2 = _y1 + 18*SCALE;
		//draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
		if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1, _y1, _x2, _y2)) {
			if (simon_led_color == c_lime && password_led_color == c_lime && wires_led_color == c_lime && device_mouse_check_button_pressed(0, mb_left)) {
				if (option_value[? "Sounds"]) {
					audio_play_sound(snd_Cut_Click, 5, false);
				}
				array_push(current_module_order, 2);
			}
		}
		type_formatted(GUI_WIDTH/2+18*SCALE, GUI_HEIGHT/2-20*SCALE, "[c_orange][fa_middle][fa_center]3");
		
		
		if (array_length(current_module_order) == 3) {
			var _correct =	module_order[0] == current_module_order[0]
							&& module_order[1] == current_module_order[1]
							&& module_order[2] == current_module_order[2];
			if (_correct) {
				//show_debug_message("You won!");
				won = true;
			}
			else {				
				//show_debug_message("KAKAKAKABOOM");
				lost_reason = "You disarmed the bomb in the wrong order.";
				lost = true;
			}
			//array_print(module_order);
			//array_print(current_module_order);
		}
		
	}
	
	
	// Phone
	if (show_phone) {		
		
		// Status bar
		draw_set_alpha(1);
		draw_sprite_ext(spr_Phone, 0, GUI_WIDTH/2, GUI_HEIGHT/2, SCALE, SCALE, 0, c_white, 1);
		var _w = sprite_get_width(spr_Phone)*SCALE;
		var _h = sprite_get_height(spr_Phone)*SCALE;
		var _x1 = GUI_WIDTH/2 - _w/2+5*SCALE;
		var _y1 = GUI_HEIGHT/2 - _h/2+5*SCALE;
		var _x2 = GUI_WIDTH/2 + _w/2-5*SCALE;
		var _y2 = _y1 + 15*SCALE;
		var _col = $6d5d50;
		var _title_text_scale = 0.8 * SCALE;
		var _date_text_scale = 0.4 * SCALE;
		var _icon_scale = 0.1 * SCALE;
		var _big_icon_scale = 1.5*_icon_scale;
		
		draw_rectangle_color(_x1, _y1, _x2, _y2, _col, _col,_col,_col, false);
		
		var _icon_signal = signal_status == SIGNAL.NO_SIGNAL ? "[$DDDDDD]"+ICON_CELL_NO_SIGNAL : (signal_status == SIGNAL.WEAK_SIGNAL ? "[#FFCC00]"+ICON_CELL_SIGNAL : "[c_green]"+ICON_CELL_SIGNAL);
		
		type_formatted(_x1, (_y1+_y2)/2, "[fnt_Phone][fa_left][fa_middle][$DDDDDD][scale,"+string(_date_text_scale)+"]"+date_date_string(date_current_datetime())+" "+date_time_string(date_current_datetime())  );
		type_formatted(_x2, (_y1+_y2)/2, "[fnt_Icons][fa_right][fa_middle][$DDDDDD][scale,"+string(_icon_scale)+"]"+_icon_signal+" [$DDDDDD]"+ICON_BATTERY+"[fnt_Phone][scale,"+string(_title_text_scale)+"]90%");
		
		var _y3 = GUI_HEIGHT/2+_h/2;
		
		//phone_screen = PHONE.ZMAIL;
		//zmail_page = 1;
		switch(phone_screen) {
			case PHONE.HOME:
				var _y_ini_icons = _y2+20*SCALE;
				var _icon_height = 30*SCALE;
				var _icon_spacing = 10*SCALE;
				draw_roundrect_color_ext(_x1+10*SCALE, _y_ini_icons, _x2-10*SCALE, _y_ini_icons + _icon_height, 12*SCALE, 12*SCALE, _col, _col, false);				
				type_formatted(_x1+16*SCALE, _y_ini_icons+6*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_MAIL+"[fnt_Phone][scale,"+string(_title_text_scale)+"] Zmail");				
				if (GUI_MOUSE_X >= _x1+16*SCALE && GUI_MOUSE_X <= _x2-10*SCALE && GUI_MOUSE_Y >= _y_ini_icons && GUI_MOUSE_Y <= _y_ini_icons+_icon_height) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						if (selected_message == -1) {
							phone_screen = PHONE.ZMAIL;
						}
						else {
							phone_screen = PHONE.ZMAIL_MESSAGE;	
						}
					}
				}
				
				var _y_ini_icons = _y_ini_icons + _icon_height+ _icon_spacing;
				draw_roundrect_color_ext(_x1+10*SCALE, _y_ini_icons, _x2-10*SCALE, _y_ini_icons + _icon_height, 12*SCALE, 12*SCALE, _col, _col, false);
				type_formatted(_x1+16*SCALE, _y_ini_icons+6*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_TIMER+"[fnt_Phone][scale,"+string(_title_text_scale)+"] Timer");				
				if (GUI_MOUSE_X >= _x1+16*SCALE && GUI_MOUSE_X <= _x2-10*SCALE && GUI_MOUSE_Y >= _y_ini_icons && GUI_MOUSE_Y <= _y_ini_icons+_icon_height) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						phone_screen = PHONE.TIMER;
					}
				}
				
				var _y_ini_icons = _y_ini_icons + _icon_height+ _icon_spacing;
				draw_roundrect_color_ext(_x1+10*SCALE, _y_ini_icons, _x2-10*SCALE, _y_ini_icons + _icon_height, 12*SCALE, 12*SCALE, _col, _col, false);
				type_formatted(_x1+16*SCALE, _y_ini_icons+6*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_SETTINGS+"[fnt_Phone][scale,"+string(_title_text_scale)+"] Settings");				
				if (GUI_MOUSE_X >= _x1+16*SCALE && GUI_MOUSE_X <= _x2-10*SCALE && GUI_MOUSE_Y >= _y_ini_icons && GUI_MOUSE_Y <= _y_ini_icons+_icon_height) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						phone_screen = PHONE.SETTINGS;
					}
				}
				
				var _y_ini_icons = _y_ini_icons + _icon_height+ _icon_spacing;
				draw_roundrect_color_ext(_x1+10*SCALE, _y_ini_icons, _x2-10*SCALE, _y_ini_icons + _icon_height, 12*SCALE, 12*SCALE, _col, _col, false);
				type_formatted(_x1+16*SCALE, _y_ini_icons+6*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_HELP+"[fnt_Phone][scale,"+string(_title_text_scale)+"] Help");
				if (GUI_MOUSE_X >= _x1+16*SCALE && GUI_MOUSE_X <= _x2-10*SCALE && GUI_MOUSE_Y >= _y_ini_icons && GUI_MOUSE_Y <= _y_ini_icons+_icon_height) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						phone_screen = PHONE.HELP;
					}
				}
				
				
				
			break;
			
			case PHONE.ZMAIL:	
				draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, _col, _col, false);
				type_formatted(_x1+5*SCALE, _y2+15*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_MAIL+"[fnt_Phone][scale,"+string(_title_text_scale)+"]Zmail");
		
				// Received messages
				var _col2 = $43312f;
				var _text_scale = 0.5 * SCALE;
		
			
				var _n = array_length(downloaded_read);
				if (_n == 0) {
					type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2, "[fnt_Phone][fa_center][fa_middle][$DDDDDD][scale,"+string(_text_scale)+"]No messages.");
				}
				var _total_pages = ceil(_n/4);
				for (var _i=zmail_page*4; _i<min(zmail_page*4+4, _n); _i++) {
					var _spacing = SCALE;
					var _y1_message = _y2+(_i-zmail_page*4+1)*43*SCALE+((_i-zmail_page*4)*_spacing);
					var _y2_message = _y1_message + 40*SCALE;
			
					draw_rectangle_color(_x1+5*SCALE, _y1_message, _x2-5*SCALE, _y2_message, _col2, _col2, _col2, _col2, false);
					
					// Go to read message (if download message is not on screen)
					if (!download_message && GUI_MOUSE_X >= _x1+5*SCALE && GUI_MOUSE_X <= _x2-5*SCALE && GUI_MOUSE_Y >=_y1_message && GUI_MOUSE_Y <= _y2_message) {
						draw_rectangle_color(_x1+5*SCALE, _y1_message, _x2-5*SCALE, _y2_message, c_white, c_white, c_white, c_white, true);
						if (device_mouse_check_button_pressed(0, mb_left)) {
							//selected_message = order[_i];
							selected_message = _i;
							downloaded_read[_i] = true;
							phone_screen = PHONE.ZMAIL_MESSAGE;
						}
					}
					//type_formatted(_x1+10*SCALE, _y1_message+10*SCALE, "[fnt_Icons_Outlined][fa_left][fa_top][$EEEEEE][scale,"+string(_icon_scale)+"]"+downloaded_read[order[_i]]+(downloaded_attachment[order[_i]] ? ICON_ATTACHMENT : "")+"[fnt_Phone][scale,"+string(_text_scale)+"] "+downloaded_subject[order[_i]]);
					var _read_icon = downloaded_read[_i] ? ICON_MAIL_READ : ICON_MAIL;
					var _col_icon = downloaded_read[_i] ? "[c_white]" : "[c_cyan]";
					type_formatted(_x1+10*SCALE, _y1_message+10*SCALE, "[fnt_Icons_Outlined][fa_left][fa_top][$EEEEEE][scale,"+string(_icon_scale)+"]"+_col_icon+_read_icon+(downloaded_attachment[_i] ? ICON_ATTACHMENT : "")+"[fnt_Phone][scale,"+string(_text_scale)+"] "+downloaded_subject[_i]);
					//type_formatted(_x1+(downloaded_attachment[order[_i]] ? 35:23)*SCALE, _y1_message+22*SCALE, "[fnt_Phone][fa_left][fa_top][$999999][scale,"+string(_text_scale)+"] From: "+downloaded_sender[order[_i]]);
					type_formatted(_x1+(downloaded_attachment[_i] ? 35:23)*SCALE, _y1_message+22*SCALE, "[fnt_Phone][fa_left][fa_top][$999999][scale,"+string(_text_scale)+"] From: "+downloaded_sender[_i]);
				}
				
				// Refresh button
				draw_roundrect_color(_x1+5*SCALE, _y3-60*SCALE, _x1+58*SCALE, _y3-42*SCALE, c_white, c_white, true);
				if (GUI_MOUSE_X >= _x1+5*SCALE && GUI_MOUSE_X <= _x1+58*SCALE && GUI_MOUSE_Y >= _y3-60*SCALE && GUI_MOUSE_Y <= _y3-42*SCALE) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						download_message = true;
					}
				}
				type_formatted(_x1+10*SCALE, _y3-45*SCALE, "[fnt_Phone][fa_bottom][fa_left][scale,"+string(_text_scale)+"][c_white]REFRESH");
				
				// Scroll arrows
				var _arrow_up_col = (zmail_page == 0 ? "[$999999]" : "[c_white]");
				var _arrow_down_col = (zmail_page == max(_total_pages-1,0) ? "[$999999]" : "[c_white]");
				var _sq_up_col = (zmail_page == 0 ? $999999 : c_white);
				var _sq_down_col = (zmail_page == max(_total_pages-1,0) ? $999999 : c_white);
				draw_roundrect_color(_x2-40*SCALE, _y3-60*SCALE, _x2-25*SCALE, _y3-42*SCALE, _sq_up_col, _sq_up_col, true);
				draw_roundrect_color(_x2-20*SCALE, _y3-60*SCALE, _x2-5*SCALE, _y3-42*SCALE, _sq_down_col, _sq_down_col, true);				
				type_formatted(_x2-5*SCALE, _y3-45*SCALE, "[fnt_Icons][fa_bottom][fa_right][scale,"+string(_icon_scale)+"]"+_arrow_up_col+ICON_UP+" "+_arrow_down_col+ICON_DOWN);
				
				if (mouse_wheel_up()) zmail_page = max(zmail_page-1, 0);
				if (mouse_wheel_down()) zmail_page = min(zmail_page+1, max(_total_pages-1,0));
				
				if (GUI_MOUSE_X >= _x2-40*SCALE && GUI_MOUSE_X <= _x2-25*SCALE && GUI_MOUSE_Y >= _y3-60*SCALE && GUI_MOUSE_Y <= _y3-42*SCALE) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						zmail_page = max(zmail_page-1, 0);
					}
				}
				if (GUI_MOUSE_X >= _x2-20*SCALE && GUI_MOUSE_X <= _x2-5*SCALE && GUI_MOUSE_Y >= _y3-60*SCALE && GUI_MOUSE_Y <= _y3-42*SCALE) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						zmail_page = min(zmail_page+1, max(_total_pages-1,0));
					}
				}
				
				if (download_message) {
					draw_set_alpha(0.5);
					draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, c_black, c_black, false);
					draw_set_alpha(1);
					draw_roundrect_color_ext(_x1+20*SCALE, _y2+70*SCALE, _x2-20*SCALE, _y3-70*SCALE, 10, 10, _col2, _col2, false);					
					draw_roundrect_color_ext(_x1+20*SCALE, _y2+70*SCALE, _x2-20*SCALE, _y3-70*SCALE, 10, 10, _col, _col, true);
					var _message_scale = 0.6*SCALE;
					var _txt = type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2-10*SCALE, "[fnt_Phone][fa_center][fa_middle][$DDDDDD][scale,"+string(_message_scale)+"]Downloading");
					var _txt = type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2-10*SCALE+_txt.bbox_height, "[fnt_Phone][fa_center][fa_middle][$DDDDDD][scale,"+string(_message_scale)+"]new messages");
					if (alarm[6] <= 0 && signal_status == SIGNAL.NO_SIGNAL) {						
						alarm[6] = time_disconnection;
						//show_debug_message("Set alarm for disconnection");
					}
					else if (alarm[6] > 0 && signal_status != SIGNAL.NO_SIGNAL) {
						alarm[6] = -1;
						//show_debug_message("Recovered signal");
					}					
					else if ( (signal_status != SIGNAL.NO_SIGNAL && alarm_downloading < 0) ) {
						//show_debug_message("Set download alarm");
						alarm_downloading = time_fast;
						downloaded_successfully = false;					
					}					
					else {			
						if (alarm[6] == -1) {
							if (signal_status == SIGNAL.FAIR_SIGNAL) {
								alarm_downloading--;
							}
							else if (signal_status == SIGNAL.WEAK_SIGNAL) {
								alarm_downloading = alarm_downloading-0.25;
							}
						}
						else {
							var _txt = type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2+60*SCALE, "[fnt_Phone][fa_center][fa_middle][c_red][scale,"+string(_message_scale/2)+"]NO SIGNAL")
							var _txt = type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2+60*SCALE+_txt.bbox_height, "[fnt_Phone][fa_center][fa_middle][c_red][scale,"+string(_message_scale/2)+"]UNSTABLE NETWORK")
						}
						
						if (signal_status != SIGNAL.NO_SIGNAL && alarm_downloading <= 0) {
							//show_debug_message("Downloaded");
							downloaded_successfully = true;
							download_message = false;
							alarm_downloading = -1;
							if (array_length(read) > 0) {
								var _time = message_time[array_length(message_time)-1]*60;
								//show_debug_message("Comparing "+string(_time)+" vs "+string(alarm[3]));
								while (_time >= alarm[3]) {
									//show_debug_message(" ok, download");
									/*
									array_push(downloaded_read, array_pop(read));
									array_push(downloaded_subject, array_pop(subject));
									array_push(downloaded_message_body, array_pop(message_body));
									array_push(downloaded_attachment, array_pop(attachment));
									array_push(downloaded_sender, array_pop(sender));
									*/
									array_insert(downloaded_read, 0, array_pop(read));
									array_insert(downloaded_subject, 0, array_pop(subject));
									array_insert(downloaded_message_body, 0, array_pop(message_body));
									array_insert(downloaded_attachment, 0, array_pop(attachment));
									array_insert(downloaded_sender, 0, array_pop(sender));
									
									array_pop(message_time);
									if (array_length(message_time) >= 1) {
										var _time = message_time[array_length(message_time)-1]*60;
										//show_debug_message("Comparing "+string(_time)+" vs "+string(alarm[3]));
									}
									else {
										_time = 0;
									}
								}
							}
							else {
								//show_debug_message("No new messages");
							}
						}
						else {
							draw_healthbar(GUI_WIDTH/2-50*SCALE, _y2+170*SCALE, GUI_WIDTH/2+50*SCALE, _y2+180*SCALE, 100*(time_fast-alarm_downloading)/(time_fast), $AAAAAA, $DDDDDD, $DDDDDD, 0, true, true);
						}
					}
					draw_circle_color(GUI_WIDTH/2, GUI_HEIGHT/2-40*SCALE, 10*SCALE, _col2, _col2, false);
					draw_circle_color(GUI_WIDTH/2, GUI_HEIGHT/2-40*SCALE, 10*SCALE, _col, _col, true);
					type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2-40*SCALE, "[fnt_Icons][fa_middle][fa_center][scale,"+string(_big_icon_scale)+"][$DDDDDD]"+ICON_CLOSE);
					if (point_in_circle(GUI_MOUSE_X, GUI_MOUSE_Y, GUI_WIDTH/2, GUI_HEIGHT/2-40*SCALE, 10*SCALE)) {
						if (device_mouse_check_button_pressed(0, mb_left)) {
							downloaded_successfully = false;
							download_message = false;
							alarm_downloading = -1;
						}
					}
				}
			break;
			
			case PHONE.ZMAIL_MESSAGE:
				draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, _col, _col, false);
				type_formatted(_x1+5*SCALE, _y2+15*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_MAIL+"[fnt_Phone][scale,"+string(_title_text_scale)+"]Zmail");
				draw_roundrect_color(_x2-20*SCALE, _y2+15*SCALE, _x2-5*SCALE, _y2+30*SCALE, c_white, c_white, true);
				
				type_formatted(_x2-5*SCALE, _y2+30*SCALE, "[fnt_Icons][fa_bottom][fa_right][scale,"+string(_icon_scale)+"][c_white]"+ICON_BACK);
				var _col2 = $43312f;
				var _y1_message = _y2+40*SCALE;
				var _y2_message = _y3-40*SCALE;
				var _text_scale = 0.5 * SCALE;
				var _body_text_scale = 0.3 * SCALE;
				draw_rectangle_color(_x1+5*SCALE, _y1_message, _x2-5*SCALE, _y2_message, _col2, _col2, _col2, _col2, false);
				var _read_icon = downloaded_read[selected_message] ? ICON_MAIL_READ: ICON_MAIL;
				type_formatted(_x1+10*SCALE, _y1_message+10*SCALE, "[fnt_Icons_Outlined][fa_left][fa_top][$EEEEEE][scale,"+string(_icon_scale)+"]"+_read_icon+(downloaded_attachment[selected_message] ? ICON_ATTACHMENT : "")+"[fnt_Phone][scale,"+string(_text_scale)+"] "+downloaded_subject[selected_message]);
				type_formatted(_x1+(downloaded_attachment[selected_message] ? 35:23)*SCALE, _y1_message+22*SCALE, "[fnt_Phone][fa_left][fa_top][$999999][scale,"+string(_text_scale)+"] From: "+downloaded_sender[selected_message]);
				
				var _m = array_length(downloaded_message_body[selected_message]);
				var _y_ini = _y1_message+40*SCALE;
				var _offset = 0;
				for (var _j=0; _j<_m; _j++) {
					var _struct = type_formatted(_x1+10*SCALE, _y_ini+_offset, "[fnt_Phone][$EEEEEE][fa_left][fa_top][scale,"+string(_body_text_scale)+"]"+downloaded_message_body[selected_message][_j]);
					_offset = _offset+_struct.bbox_height;
				}
				
				// Back
				if (GUI_MOUSE_X >= _x2-20*SCALE && GUI_MOUSE_X <= _x2-5*SCALE && GUI_MOUSE_Y >= _y2+15*SCALE && GUI_MOUSE_Y <= _y2+30*SCALE) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						selected_message = -1;
						phone_screen = PHONE.ZMAIL;
					}
				}
			break;
			
			case PHONE.TIMER:
				draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, _col, _col, false);
				type_formatted(_x1+5*SCALE, _y2+15*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_TIMER+"[fnt_Phone][scale,"+string(_title_text_scale)+"]Timer");
				
				var _total_seconds = floor(alarm[3]/60);
				var _minutes = floor(_total_seconds/60) < 10 ? "0"+string(floor(_total_seconds/60)) : string(_total_seconds/60);
				var _seconds = _total_seconds % 60 < 10 ? "0"+string(_total_seconds % 60) : string(_total_seconds % 60);
				var _timer_color = "[c_white]";
				
				if (alarm[3] % 60 < 30) {
					var _alpha = "[alpha,0]";
				}
				else {
					var _alpha = "[alpha,1]";
				}
				type_formatted(GUI_WIDTH/2, GUI_HEIGHT/2, "[fnt_Phone][fa_center][fa_middle][scale,"+string(2*SCALE)+"]"+_alpha+_timer_color+_minutes+":"+_seconds+"[alpha,1]", true, false);
				
			break;
			
			case PHONE.SETTINGS:
				var _text_scale = 0.5 * SCALE;
				
				draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, _col, _col, false);
				type_formatted(_x1+5*SCALE, _y2+15*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_SETTINGS+"[fnt_Phone][scale,"+string(_title_text_scale)+"]Settings");
				
				var _y1_message = _y2+40*SCALE;
				var _icon = option_value[? "Music"] ? ICON_CHECKBOX_ON : ICON_CHECKBOX_OFF;
				type_formatted(_x1+10*SCALE, _y1_message+10*SCALE, "[fnt_Icons_Outlined][fa_left][fa_top][$EEEEEE][scale,"+string(_icon_scale)+"]"+_icon+"[fnt_Phone][scale,"+string(_text_scale)+"] Music");
				//draw_rectangle(_x1+10*SCALE, _y1_message+10*SCALE, _x1+80*SCALE, _y1_message+20*SCALE,false);
				if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1+10*SCALE, _y1_message+10*SCALE, _x1+80*SCALE, _y1_message+20*SCALE)) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						option_value[? "Music"] = !option_value[? "Music"];
						if (option_value[? "Music"]) {							
							audio_resume_sound(music_sound_id);
						}
						else {
							audio_pause_sound(music_sound_id);
						}
					}
				}
				var _y1_message = _y1_message + 20*SCALE;
				var _icon = option_value[? "Sounds"] ? ICON_CHECKBOX_ON : ICON_CHECKBOX_OFF;
				type_formatted(_x1+10*SCALE, _y1_message+10*SCALE, "[fnt_Icons_Outlined][fa_left][fa_top][$EEEEEE][scale,"+string(_icon_scale)+"]"+_icon+"[fnt_Phone][scale,"+string(_text_scale)+"] Sounds");
				//draw_rectangle(_x1+10*SCALE, _y1_message+10*SCALE, _x1+80*SCALE, _y1_message+20*SCALE,false);
				if (point_in_rectangle(GUI_MOUSE_X, GUI_MOUSE_Y, _x1+10*SCALE, _y1_message+10*SCALE, _x1+80*SCALE, _y1_message+20*SCALE)) {
					if (device_mouse_check_button_pressed(0, mb_left)) {
						option_value[? "Sounds"] = !option_value[? "Sounds"];
						if (option_value[? "Sounds"]) {							
							audio_resume_sound(sound_heartbeat);
						}
						else {
							audio_pause_sound(sound_heartbeat);
						}
					}
				}
			break;
			
			case PHONE.HELP:
				draw_roundrect_color(_x1, _y2+10*SCALE, _x2, _y3-25*SCALE, _col, _col, false);
				type_formatted(_x1+5*SCALE, _y2+15*SCALE, "[fnt_Icons][fa_left][fa_top][$EEEEEE][scale,"+string(_big_icon_scale)+"]"+ICON_HELP+"[fnt_Phone][scale,"+string(_title_text_scale)+"]Help");
				
			break;
			
		}
		// Home
		if (phone_screen != PHONE.HOME) {
			var _col2 = $43312f;
			draw_circle_color(GUI_WIDTH/2, _y3-20*SCALE, 15*SCALE, _col2, _col2, false);
			draw_circle_color(GUI_WIDTH/2, _y3-20*SCALE, 15*SCALE, c_white, c_white, true);
			type_formatted(GUI_WIDTH/2, _y3-10*SCALE, "[fnt_Icons][fa_bottom][fa_center][scale,"+string(_big_icon_scale)+"][c_white]"+ICON_HOME);
			if (point_in_circle(GUI_MOUSE_X, GUI_MOUSE_Y, GUI_WIDTH/2, _y3-20*SCALE, 15*SCALE)) {
				if (device_mouse_check_button_pressed(0, mb_left)) {
					phone_screen = PHONE.HOME;
				}
			}
		}
	}
	fnc_RestoreDrawParams();
}


function fnc_Strike() {
	display_strike = true;
	strikes++;				
	moment_of_strike = alarm[3];
	alarm[9] = 120;
}


#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = GUI_MOUSE_X >= _bbox_coords[0] && GUI_MOUSE_Y >= _bbox_coords[1] && GUI_MOUSE_X <= _bbox_coords[2] && GUI_MOUSE_Y <= _bbox_coords[3];
	//var _mouseover = GUI_MOUSE_X >= _draw_data_normal.bbox_x1 && GUI_MOUSE_Y >= _draw_data_normal.bbox_y1 && GUI_MOUSE_X <= _draw_data_normal.bbox_x2 && GUI_MOUSE_Y <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			//script_execute(_callback, _param);
			_callback(_param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	var _function = (asset_get_index("fnc_Menu_"+string(_param)));
	_function();
}

function fnc_ExecuteOptions(_param) {
	var _function = script_execute(asset_get_index("fnc_Options_"+string(_param)));
	_function();
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.5);
		//draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(20, 20,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(20, 30, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.state);
		}
		else {
			draw_text(20, 30, "Player not in room");	
		}
		var _spacing = 50;
		
		var _debug_data = [];
		
		array_push(_debug_data, "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")");
		array_push(_debug_data, "Room speed: "+string(room_speed)+" Game speed: "+string(gamespeed_fps));
		array_push(_debug_data, "Requested scaling type: "+string(SELECTED_SCALING)+" ("+
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW ? "Normal mode (resolution scaled to window)" : 
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW ? "Resolution independent of window, unscaled" : "Resolution independent of of window, scaled")+")"));
		array_push(_debug_data, "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H));
		array_push(_debug_data, "Actual base resolution: "+string(adjusted_resolution_width)+"x"+string(adjusted_resolution_height));
		array_push(_debug_data, "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H));
		array_push(_debug_data, "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height));
		array_push(_debug_data, "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")");
		array_push(_debug_data, "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")");
		array_push(_debug_data, "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with GM function): "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with helper functions!): "+string(GUI_WIDTH)+"x"+string(GUI_HEIGHT)+" ("+string(round(GUI_WIDTH/GUI_HEIGHT * 100)/100)+")");
		array_push(_debug_data, "Mouse: "+string(mouse_x)+","+string(mouse_y));
		array_push(_debug_data, "Device mouse 0: "+string(GUI_MOUSE_X)+","+string(GUI_MOUSE_Y));
		array_push(_debug_data, "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0)));
		array_push(_debug_data, "Paused: "+string(paused));
		array_push(_debug_data, "FPS: "+string(fps_real) + "/" + string(fps));
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(20, _spacing+_i*15, _debug_data[_i]);
			//show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion
