#region Documentation
/***********************************************************************************************************************
	
											___        _                         
											 |  ._  _ |__ ._._ _  _._|__|_ _  _| 
											 |\/|_)(/_|(_)| | | |(_| |_ |_(/_(_| 
											  / |                                
	
							The amateur developer's version of JujuAdams's fabulous Scribble asset.
										"Bad performance, but it works in HTML5!"


											by José Alberto Bonilla Vera (biyectivo)
												www.biyectivo.com/typeformatted
										biyectivo@gmail.com / Discord tag biyectivo#2771
	
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	-------
	 INTRO
	-------
	
	TypeFormatted is a simple asset that allows you to draw formatted text and inline sprites in a single call. It's
	heavily inspired by the marvelous JujuAdams's Scribble asset (https://github.com/JujuAdams/Scribble). It performs
	WAY slower than Scribble and it is much less professionally written, but the main reason I developed it is because
	Scribble does not currently work on HTML5 exports due to a bug in Gamemaker's Javascript runner, and it seems
	there's no short-term solution on the horizon.
	
	While Scribble does vertex buffer magic under the hood, TypeFormatted simply combines Gamemaker's built-in functions
	to draw formatted text for you.
	
	TypeFormatted supports different formatting tags for changing color, alignment, alpha, blend, font, inline sprites,
	text blinking and a basic typewriter animation.
	
	TypeFormatted does NOT currently support multiline text, sounds and other effects, and it's unlikely it will ever
	support those. If you need fancy effects, custom events etc. you should use Scribble, unless you are targeting
	HTML5, in which case you'll need to find another way.
	
	It also currently supports limited rotated drawing for text and inline sprites, but with many bugs.
	
	
	---------
	 CAVEATS
	---------
	
	Depending on the number of strings drawn, TypeFormatted WILL drop your framerate significantly, because it uses 
	built-in functions to draw and data structures to parse. Also, it is currently not very optimized.
	
	TypeFormatted requires Gamemaker Studio 2.3 or greater.
	
	Check the Known Bugs section before using this.
	
	Use this asset at your own risk (and performance drop). If you don't need an HTML5 export, I highly recommend
	using JujuAdams's Scribble instead.
	
	
	-------
	 USAGE
	-------
	
	1) Add obj_TypeFormatted to your init/first room.
	2) Use the following function in a Draw or Draw GUI event:
	
	
			_mystruct = type_formatted(x, y, string <,draw, cache, blink_speed, start_character, speed>);
	
	------------
	 PARAMETERS
	------------
	
	x - x position to draw
	y - y position to draw
	string - text string with different format tags, as such:
		
		\\							Escape character. The following character will be interpreted verbatim.
		
									Example:
									\\[					(Will print an open square bracket)
									
									Note, you cannot escape a backward slash at the moment - use [chr,92] instead
									(see [chr,xxxxxx] below).
		
		[]							Reset formatting to default values. More specifically, immediately reset font,
									color and alpha.
									
									Note that at the start of each string draw, other values (scale, blend, angle...)
									are also reset automatically.
									
									You can set the default formatting options by modifying the corresponding macro
									values in scr_TypeFormatted_Helpers (see below).
									
		[c_xxxxxxxx]				Change color of the following characters or sprite's blend color, by using 
									etiher a Gamemaker color constant (c_...) or the additional color constants
									included in this asset.
									Check out the list of additional colors available in obj_TypeFormatted's 
									User Event 0.
							
									Examples:
									[c_red]				(Gamemaker's built-in red constant)
									[c_dksalmon]		(TypeFormatted's dark salmon constant)
									
		[c_random]					Choose a random color (every frame!) to draw the following character.
		
		[$XXXXXX]					Change color of the following characters using a Gamemaker's custom color (BGR).
									Use uppercase letters.
									
									Examples:
									[$00CCFF]			(Gamemaker's gold color code)
									
		[#XXXXXX]					Change color of the following characters using the traditional hex color format (RGB).
									Use uppercase letters.
		
									Examples:
									[#FFCC00]			(Hex gold color code)
									
		[angle, x]					Change the angle at which following text or sprites are drawn, x between
									0 and 360.
							
									Examples:
									[angle, 45]			(Draw at a 45° angle)
									
		[blend, xxxxxx]				Change blend color of the following sprites using either a Gamemaker's built-in 
									color constant, a Gamemaker's custom BGR color or a hex color.
									
									Examples:
									[blend, #999999]	(Draw sprites using the hex gray color)
		
		[alpha, x]					Change alpha of the following text or sprites; x must be between 0 and 1.
							
									Examples:
									[alpha, 0.8]		(Draw at 80% opacity)
									
		[scale, x <,y>]				Change scale of the following text or sprites; specify x only to scale 
									proportionaly both width and height; specify x and y to scale at different
									proportions.
							
									Examples:
									[scale, 2]			(Draw at 2x size proportionally)
									[scale, 1.5, 3]		(Draw width scaled at 1.5x and height scaled at 3x)
		
		[chr, x]					Insert a character based on its ASCII code x, where x must be a valid ASCII
									code (32-126). Extended ASCII codes are not supported.
									
									Example:
									[chr,92]			(Draw a single backslash character)
		
		[fa_xxxxxx]					Change horizontal or vertical alignment for the entire string, using the 
									Gamemaker's built-in alignment constants. If multiple horizontal or vertical
									tags are present in the string, only the last horizontal alignment and vertical
									alignment tags are taken into account.
									
									Examples:
									[fa_bottom]			(Draw everything vertically aligned to the bottom baseline)
									[fa_right]			(draw everything horizontally aligned to the right)
									
		[sprite <,idx, speed>]		Draw the requested sprite inline with the rest of the text. If only the sprite's
									name is used, it will draw frame 0 of the sprite, without animation; if a starting
									frame is specified, it will draw that frame instead and animate using the sprite
									speed defined in the sprite properties dialog; finally, if both a starting frame 
									and speed are specified, it will animate the sprite, starting at the specified 
									frame and animating every <speed> game steps.
									
									Examples:
									[spr_Player]		(Draw the first frame of the spr_Player sprite, statically)
									[spr_Player,2]		(Draw the third frame of the spr_Player sprite, animating
														at the speed set in the sprite properties)
									[spr_Player,2,8]	(Draw the spr_Player sprite and animate it, starting with
														frame 2 and animating it every 8 game steps, i.e., at
														8/60 frames per second for a game with Game Speed set at 60)
		

	<draw>				optional boolean argument that sets whether to actually draw the string or just add it to 
						the cache. The default is true.
	<cache>				optional boolean arguments that sets whether the string is cached. By default, it's true.
	<blink_speed>		optional integer argument that determines the blink speed of the text. The default is 0
						(no blink). Note it's not currently possible to setup a blink for only part of a string.
						If you need to do this, you will need to use more than one type_formatted call.
	<start_character>	optional nonnegative integer argument for typewriter mode; sets the index of the character to start
						drawing at. Both start_character and speed need to be set, if the typewriter effect is to be
						used.
	<speed> 			optional positive integer argument for typewriter mode; sets the speed in game steps to draw 
						text at. Both start_character and speed need to be set, if the typewriter effect is to be used.
	
	
	--------
	 OUTPUT
	--------
	
	A struct (of type global.TypeFormatted_ParsedElement) containing a lot of info.
	What is usable about the output struct is the following:
	
	"Public" Attributes:
	
	bbox_width - The total width of the bounding box of the parsed string.
	bbox_height - The total height of the bounding box of the parsed string.
	halign - The horizontal alignment considered for the parsed string.
	valign - The vertical alignment considered for the parsed string.
	
	
	"Public" Methods:
	-----------------
	
	setTypewriterDelay(x)	Sets the typewriter effect's delay between each character/sprite, x being game steps.
	restartTypewriter()		Restarts the typewriter at the first character/sprite.
	endTypewriter()			Ends the typewriter effect and renders the complete string.		
	pauseTypewriter()		Pauses the typewriter effect
	resumeTypewriter()		Resumes the typewriter effect
	getTypewriterStatus()	Returns an enum of type TYPE_FORMATTED_TYPEWRITER with the following values:
							TYPEWRITER_DISABLED - The string has no typewriter effect set
							TYPEWRITER_PAUSED - The typewriter effect is paused
							TYPEWRITER_RUNNING - The typewriter effect is running
							TYPEWRITER_ENDED - The typewriter effect has concluded
	startBlink()			Start blinking for that string.
	stopBlink()				Stop blinking for that string.
	totalCharacters()		counts the number of characters in a parsed string, adding 1 for each sprite.
	debugString()			Prints the parsed string in Gamemaker's log
	bbox(x, y)				Return an array with the x1, y1, x2 and y2 coordinates of the bounding box, given
							the x and y positions where the string was/is going to be drawn.
	
	
	----------
	 EXAMPLES
	----------
	
	
	
	-------
	 CACHE
	-------
	
	TypeFormatted uses a ds_map to cache parsed strings in order to attempt to speed up the drawing each frame.
	However, if the cache gets too big, the balance between the performance gain to parse the string and
	the performance loss given by the size of the map will be broken. You can set the maximum size of the 
	cache and let TypeFormatted flush it automatically at that size, or you can also flush the cache manually.
	You can also determine whether a specific string is cached or not using the parametr in the function
	call.
	
	To set the maximum size of the cache and let TypeFormatted flush the cache automatically:
	
	1) Set the macro TYPE_FORMATTED_AUTOFLUSH to true, within scr_TypeFormatted_Helpers.
	2) Set the macro TYPE_FORMATTED_CACHE_SIZE_FLUSH to the desired limit, within scr_TypeFormatted_Helpers.
	
	To manually flush the cache:
	
	1) Call fnc_TypeFormatted_Flush() whenever needed.
	
	To specify a specific string should not be cached:
	
	1) Set the [cache] parameter to false in the type_formatted() call.
	
	Important:
	
	If you are drawing a lot of dynamically built strings (game score, FPS, ship speed...), you'll want to either
	flush the cache (either automatically or manually) or specify that the string should not be cached; this is
	because, in these cases, by default every string will be different and thus will be parsed and added to the 
	cache individually.
	
	You can also get the size of the cache by calling fnc_TypeFormatted_CacheSize();
	
	Note that flushing the cache will reset all effects (i.e. it will restart the typewriter of other strings being
	draw in the same event, it will restart blinking, etc.)
	
	------------------------------
	 OTHER USEFUL MACRO VARIABLES
	------------------------------
	
	Default draw settings:
	
	TYPE_FORMATTED_DEFAULT_ALPHA 				Default value for alpha
	TYPE_FORMATTED_DEFAULT_COLOR 				Default value for color
	TYPE_FORMATTED_DEFAULT_HALIGN				Default value for horizontal alignment
	TYPE_FORMATTED_DEFAULT_VALIGN				Default value for vertical alignment
	TYPE_FORMATTED_DEFAULT_XSCALE				Default value for x scaling
	TYPE_FORMATTED_DEFAULT_YSCALE				Default value for y scaling
	TYPE_FORMATTED_DEFAULT_FONT					Default font. If you are using a base font for you project, it's 
												strongly recommended to set the default font to your font, to avoid
												unnecesary swaps.
	
	Other settings:
	
	TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE		Threshold under which an alpha change will not be processed.
												For example, if this value is 0.05 and you change alpha from 1.0 to
												0.96, since the difference is less than 0.05 it will not be changed, to
												avoid additional swaps.
	TYPE_FORMATTED_DEBUG_BBOX					Set to true to visually see the bounding box overlaid in top of your
												drawn string.
	TYPE_FORMATTED_DEBUG_NOTES					Set to true to display notes in Gamemaker's log.
	TYPE_FORMATTED_DEBUG_VERBOSE				Set to true to display (lots of) debugging info (parsing, drawing etc.)
												in Gamemaker's log.
	
	------------
	 KNOWN BUGS
	------------
	
	* Attempting to draw rotated text will draw each part of the text starting on the baseline (depending on vertical
	alignment) and not using the rotated baseline as it should.
	
	------------
	  LICENSE
	------------
	
	This work is licensed as Creative Commons Attribution-No Derivatives 4.0 International
	https://creativecommons.org/licenses/by-nd/4.0/legalcode
	
	What does this mean?
	
	Check out: https://creativecommons.org/licenses/by-nd/4.0/
	
	This means you can copy and redistribute the material in any medium or format for any purpose, even commercially,
	and that you must give appropriate credit, provide a link to the license, and indicate if changes were made.
	You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
	Also if you remix, transform, or build upon the material, you may not distribute the modified material.
	You may not apply legal terms or technological measures that legally restrict others from doing anything the license
	permits.
	
************************************************************************************************************************/
#endregion


///@function		type_formatted(x, y, string, [draw], [blink speed], [start_character], [speed])
///@description		generates (and optionally draws) formatted text to screen, given a position and a tag-formatted string.
///@param			x				x coordinate
///@param			y				y coordinate
///@param			string			Tag-formatted string
///[@param]			draw			Optional; draw [true=default] parse [false]
///[@param]			cache			Optional; cache [true=default] or not [false]
///[@param]			blink			Optional; blinking speed [0=no blinking]
///[@param]			start_character	Optional; start typewriter at this char [0=no typewriter]
///[@param]			speed			Optional; set typewriter speed
///@return			A struct containing some attributes and methods, including the bbox width/height and typewriter effect methods. See the full documentation.

function type_formatted(_x, _y, _string/*, _draw, _cache, _blink_speed, _start_character, _speed*/) {
	
	var _actually_draw = true;
	if (argument_count > 3) {
		var _actually_draw = argument[3];	
	}
	var _cache = true;
	if (argument_count > 4) {
		var _cache = argument[4];	
	}
	var _blink_speed = 0;
	if (argument_count > 5) {
		var _blink_speed = argument[5];	
	}
	var _start_character = 0;
	if (argument_count > 6) {
		var _start_character = argument[6];
	}
	var _typewriter_delay = -1;
	if (argument_count > 7) {
		_typewriter_delay = argument[7];
	}
	
	
	// Parse string if not already parsed
	var _struct = fnc_TypeFormatted_FindParsedString(_string);
	if (_struct == undefined) {		
		type_formatted_create(_x, _y, _string, _blink_speed, _start_character, _typewriter_delay);
		_struct = fnc_TypeFormatted_FindParsedString(_string);
	}
	
	// Render string
	if (_actually_draw) {
		type_formatted_draw(_x, _y, _string, _cache);
	}
	
	// Return result struct
	return _struct;
}


///@function		fnc_TypeFormatted_Flush()
///@description		flushes the TypeFormatted cache.

function fnc_TypeFormatted_Flush() {
	if (ds_exists(global.TypeFormatted_parsedElementMap, ds_type_map)) {
		ds_map_destroy(global.TypeFormatted_parsedElementMap);		
		global.TypeFormatted_parsedElementMap = ds_map_create();
		if (TYPE_FORMATTED_DEBUG_NOTES) {
			show_debug_message("=== [TypeFormatted/fnc_TypeFormatted_Flush]: NOTE: Flushed TypeFormatted cache");
		}
	}	
}

///@function		fnc_TypeFormatted_CacheSize()
///@description		gets the TypeFormatted cache size.
///@return			The size of the cache, or -1 if not initialized.

function fnc_TypeFormatted_CacheSize() {
	if (ds_exists(global.TypeFormatted_parsedElementMap, ds_type_map)) {
		return ds_map_size(global.TypeFormatted_parsedElementMap);
	}
	else {
		return -1;
	}
}