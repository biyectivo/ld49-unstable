// Function to (re)initialize game variables
function fnc_InitializeGameStartVariables() {
	
	// Generic
	
	paused = false;
	lost = false;
	won = false;
	
	if (room == room_Game_1) {
		total_time_taken = 0;
	}
	lost_reason = "";
	
	level = 1;
	total_score = 0;
	death_message_chosen = "";
	
	current_step = 0;	
	
	scoreboard_queried = false;
	scoreboard = [];
	scoreboard_html5 = [];
		
	http_get_id_query = noone;
	http_return_status_query = noone;
	http_return_result_query = noone;
	
	http_get_id_update = noone;
	http_return_status_update = noone;
	http_return_result_update = noone;
		
	current_scoreboard_updates = 0;
	max_scoreboard_updates = 1;
	timer_scoreboard_updates = 180;
		
	// Reset alarms (except for 0, which is center screen), since Game is persistent
	for (var _i=1; _i<=11; _i++) {
		Game.alarm[_i] = -1;
	}
	
	fullscreen_change = false;
	
	// Swipe
	swiping = array_create(4, false);
	swipe_start = array_create(4, -1);
	swipe_end = array_create(4, -1);
	swipe_distance = array_create(4, -1);
	swipe_time = array_create(4, -1);
	swipe_started_now = array_create(false);
	swipe_ended_now = array_create(false);
	
	
	// Phone screen
	
	enum PHONE {
		HOME,
		ZMAIL,
		ZMAIL_MESSAGE,
		TIMER,
		GAME,
		SETTINGS,
		HELP
	}
	phone_screen = PHONE.HOME;
	show_phone = false;
	
	

	// Bomb timer
	
	bomb_timer = 60*(60*10);
	alarm[3] = bomb_timer;
	
	// Signal booster
	
	y1_signal_booster = 20;
	height_red = 40;
	height_yellow = 20;
	height_green = 30;
	y_knob = y1_signal_booster + 2*height_red + 2*height_yellow + height_green;
	change_signal_booster_timer = 120;
	alarm[4] = change_signal_booster_timer;
	delta_red = 0;
	delta_yellow = 0;
	delta_green = 0;
	
	enum SIGNAL {
		NO_SIGNAL,
		WEAK_SIGNAL,
		FAIR_SIGNAL
	}
	
	signal_status = SIGNAL.FAIR_SIGNAL;
	time_fast = 60 * 2;
	//prob_disconnection_yellow = 0.4;
	time_disconnection = 60 * 2;
	alarm_downloading = -1;
	
	// Bomb password
	show_bomb = false;
	four_letter_password = [irandom_range(65,90),irandom_range(65,90),irandom_range(65,90),irandom_range(65,90)];
	four_letter_password_current = [ord("A"), ord("A"), ord("A"), ord("A")];
	
	// Bomb temperature
	bomb_temperature = 0;
	max_bomb_temperature = 35;
	max_temperature = 40;
	
	button_mash = [];
	button_mash_speed = [];
	
	// Simon says
	simon_colors = [c_blue, c_red, c_lime, c_yellow, c_fuchsia, c_aqua];
	simon_says = [];
	for (var _i=0; _i<5; _i++) {
		array_push(simon_says, irandom_range(0, 5));
	}
	simon_speed = 20;
	current_simon = [];
	simon_light = -1;
	simon_on = false;
	current_simon_length = 0;
	current_simon_length_light_number = -1;
	currently_drawing_simon_light = false;
	
	enum WIRES {
		BLUE,
		GREEN,
		RED,
		YELLOW
	}
	wire_names = ["blue","green","red","yellow"];
	
	var _format_for_mail = "[c_cyan]";
	// Wires
	wires_to_cut = [];
	num_wires_to_cut = irandom_range(2,3);
	var _wire_string_except_last_one = "";
	for (var _i=0; _i<num_wires_to_cut; _i++) {
		var _found = 0;
		while (_found != -1) {
			var _wire = irandom_range(0,3);
			_found = array_find(wires_to_cut, _wire);
		}
		if (_i<num_wires_to_cut-1) {
			_wire_string_except_last_one = _wire_string_except_last_one + _format_for_mail+wire_names[_wire]+"[c_white] then ";
		}
		array_push(wires_to_cut, _wire);
	}
	_wire_string_except_last_one = _wire_string_except_last_one + _format_for_mail+" ?";
	
	actual_wires_cut = [];
	
	// Strikes
	strikes = 0;
	display_strike = false;
	moment_of_strike = 0;
	
	// Bomb disarm sequence order
	module_order = [];
	current_module_order = [];
	var _n = 3;
	for (var _i=0; _i<_n; _i++) {
		var _found = 0;
		while (_found != -1) {
			var _module = irandom_range(0,2);
			_found = array_find(module_order, _module);
		}		
		array_push(module_order, _module);
	}
	
	// Sounds
	alarm[10] = 30;	
	sound_heartbeat = noone;
	sound_beep = noone;
	sound_explosion = noone;
	
	// Mail
	downloaded_read = [];
	downloaded_sender = [];
	downloaded_subject = [];
	downloaded_message_body = [];	
	downloaded_attachment = [];
	
	
	// Email
	sender = [	"Joe Green",
				"LDJAM.COM", 
				"Joe Green",
				"Linda",
				"Joe Green",
				"NG Lottery",
				"Joe Green",
				"Mark W",
				"Mark W",
				"Joe Green",
				"Mark W",
				"Joe Green",
				"Joe Green",
				"Linda",
				"Joe Green",
				"Joe Green",
				"Bradley, Amy",
				"Joe Green"];
				
	subject = [	"RE: URGENT HELP, BOMB",
				"Ludum Dare 49 Theme",
				"Bomb",
				"Where are you?",
				"First bomb module",
				"You won!", 
				"Bomb temperature",
				"Golf pics 1/3",
				"Golf pics 2/3", 
				"PASSWORD",
				"Golf pics 3/3",
				"bomb wires",
				"Disarm sequence",
				"RE: Where are you?",
				"RE: bomb wires",
				"PASSWORD last letter",
				"Reminders",
				"LAST WIRE..."];
				
	message_time = [60*10,
					60*9.5,
					60*9.5,
					60*9,
					60*9,
					60*8.5,
					60*8.5,
					60*8,
					60*7.5,
					60*7,
					60*7,
					60*6.5,
					60*5,
					60*4.5,
					60*4,
					60*2.5,
					60*2.25,
					60*1.5,];
	
	
	message_body = [
					["Mr. Robertson, received your message.",
						"I'm already researching the bomb.",
						"Focus on the bomb, I'll send you ",
						"bits of info as soon as possible, ",
						"[c_cyan]check your email often.",
						" ",						
						"Sorry we can't talk, but the ",
						"signal is super unstable down there,",
						"to improve the signal use the ",
						"signal booster at the upper right corner",
						"and use the [c_cyan]Q and A keys",
						"to center the antenna.",
						" ",
						"If you don't center the antenna,",
						"you will have no signal and won't",
						"be able to download your emails.",
						" ",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["The theme for the 49th Ludum Dare is..."," ","[c_cyan]UNSTABLE"," ","(sorry for this!). Good luck!"],					
					["Mr. Robertson, using the security",
						"camera I was able to get info on.",
						"the bomb. There are three modules,",
						"you will need to disable all three",
						"in a specific order. I'm currently",
						"checking what the correct order is,",
						"hold on until I have more data!",
						" ",
						"I have already deduced something -",
						"the fourth letter of the password",
						"on the bomb module. It is",
						"[c_cyan]"+chr(four_letter_password[3])+"[c_white].",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Baby, where are you??? I'm looking",
						"for you and am worried!",
						"Please reply, kisses,",
						" ",
						"-Linda"],
					["Mr. Robertson, there's a module on",
						"the bomb that works like a 'Simon",
						"Says'. [c_cyan]Turn it on[c_white] with the flick",
						"switch and then repeat the",
						"patterns. If you think you don't",
						"remember the pattern at any",
						"point, you can reset the whole",
						"sequence by turning the switch",
						"on and off.",
						" ",
						"There's a LED that turns green",
						"when you've done it correctly",
						"- hopefully you do it right.",
						"Be careful!!!",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["You won the Nigerian lottery!","The winning ticket number was:","[c_cyan]000049","Find attached your receipt","Claim your prize online:","[c_cyan]www.lotto.ng"," ","###ZAntivirus flagged this message"," as suspicious###"],
					["Mr. Robertson, just read the bomb MUST",
						"be [c_cyan]kept below "+string(max_bomb_temperature)+"°C!",
						" ",
						"There's a ventilation control in ",
						"the plutonium room, but you'll have to",
						"[c_cyan]alternate mashing your Z and X keys",
						"repeatedly, to make it work.",
						"Check the bomb temperature module to",
						"make sure it's working!",
						" ",
						"This will only reduce the temperature",
						"for a while, but you can always repeat",
						"if necessay. If you exceed the max,",
						"[c_cyan]the bomb might become unstable",
						"and explode, so it is critical that you",
						"keep it controlled.",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Hey buddy, find attached the golfing",
						"pics we took at Mayakoba!",
						" ",
						"Sorry for the email spam, but ",
						"you know I don't know how to",
						"upload them to that cloud thing...",
						" ",
						"I split the pics into three",
						"emails, they're on your way!",
						" ",
						"- Mark"],
					["Sup, more pics, 2 of 3",
						" ",
						"- Mark"],
					["Mr. Robertson, I figured out the third",					
						"and first letters of the password:",
						"[c_cyan]"+chr(four_letter_password[2])+"[c_white] and [c_cyan]"+chr(four_letter_password[0])+".",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Sup, final mail with pics.",
						"Let's play again soon!",
						"Say hello to the wife!",
						" ",
						"- Mark"],
					["Mr. Robertson, I have info on the wires:",
						" ",
						"1. They need to be cut IN ORDER",
						"2. You need to cut "+string(num_wires_to_cut)+" wires",
						" ",
						"Don't do anyting until I figure out",
						"more...",						
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Mr. Robertson, once you have disabled",
						"the three modules, you'll have to",
						"press the disarm buttons below the",
						"timer in this order:",
						" ",
						"[c_cyan]"+string(module_order[0]+1)+" -> "+string(module_order[1]+1)+" -> "+string(module_order[2]+1),
						" ",
						"This will (hopefully) disarm the bomb...",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Harry, I'm really starting to freak",
						"out. ANSWER ME PLEASE!!!",
						"If you don't reply in five, I'm",
						"calling the police.",
						" ",
						"-Linda"],
					["Mr. Robertson, I nearly have all info",
						"on the wires!!! This is the order,",
						"I'm still missing the last one",
						"though:",
						" ",
						_wire_string_except_last_one,
						" ",
						"I'll get the last one ASAP",
						" ",						
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Mr. Robertson, found the last remaining",
						"letter of the password (was it the",
						"third or the second one though?",
						"it is:",
						" ",
						"[c_cyan]"+chr(four_letter_password[1]),						
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
					["Sir, just reminding you of",
						"the urgent Kalypso Co. board",
						"meeting tomorrow at 8am.",
						" ",
						"Also, the environmental",
						"activists were causing yet",
						"another disturbance outside",
						"Obviously I sent for Security",
						"immediately, but they surely are",
						"some annoying people.",
						"Maybe we should reply to their",
						"petition letters? It's the",
						"fifth time they've written.",
						" ",
						"Anyway, see you sir,",
						" ","Amy Bradley","Executive Assistant","genEnergy Inc."],
					["Mr. Robertson, I figured out how",
						"to get the last wire!!!",
						"However, you will need to",
						"check it yourself.",
						" ",
						"It is based on the genEnergy",
						"HZ8374 proprietary algorithm.",
						"If you go into the room [c_cyan]directly",
						"[c_cyan]below the plutonium storage[c_white], you",
						"can [c_cyan]find a diagram there[c_white] on the",
						"wall...",
						" ",
						"BEST OF LUCK!!!",
						" ","Best,","Joe Green","Nuclear Power Plant Director","genEnergy Inc."],
				];
					
	
	attachment = [false, false, false, false, false, true, false, true, true, false, true, false, false, false, false, false, false, false];				
	
	/*
	show_debug_message(array_length(sender));
	show_debug_message(array_length(subject));
	show_debug_message(array_length(message_body));
	show_debug_message(array_length(message_time));
	show_debug_message(array_length(attachment));
	
	
	array_print(simon_says);
	array_print(wires_to_cut);
	show_debug_message(num_wires_to_cut);
	show_debug_message(_wire_string_except_last_one);
	show_debug_message(chr(four_letter_password[0])+" "+chr(four_letter_password[1])+" "+chr(four_letter_password[2])+" "+chr(four_letter_password[3]));
	show_debug_message(max_bomb_temperature);
	
	array_print(module_order);
	*/
	
	var _n = array_length(sender);
	read = array_create(_n, false);
	read_action = array_create(_n, function() {});	
	
	sender = array_reverse(sender);
	subject = array_reverse(subject);
	message_time = array_reverse(message_time);
	message_body = array_reverse(message_body);
	attachment = array_reverse(attachment);
	
	
	
	
	
	/*
	downloaded_read = read;
	downloaded_sender = sender;
	downloaded_subject = subject;
	downloaded_message_body = message_body;	
	downloaded_attachment = attachment;
	*/
		
	
	order = [];
	
	for (var _i=0; _i<_n; _i++) {
		//order[_i] = _n-_i-1;
		order[_i] = _i;
	}
	
	
	
	zmail_page = 0;
	selected_message = -1;
	download_message = false;
	
	show_diagram = false;
	
	
	simon_led_color = c_red;
	wires_led_color = c_red;
	password_led_color = c_red;
	
	
	
	
	array_push(downloaded_read, array_pop(read));
	array_push(downloaded_subject, array_pop(subject));
	array_push(downloaded_message_body, array_pop(message_body));
	array_push(downloaded_attachment, array_pop(attachment));
	array_push(downloaded_sender, array_pop(sender));
	array_pop(message_time);
}


// Update scoreboard
function fnc_UpdateScoreboard() {
	if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
		var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/score_insert.php";
		var _hash = sha1_string_utf8(Game.option_value[? Game.option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
		var _params = "user="+Game.option_value[? Game.option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
		
		http_get_id_update = http_get(_scoreboard_url + "?" + _params);
	}
	else {
		http_get_id_update = -1;
	}
}


/// @function fnc_ChooseProb(choose_array, array_probs)
/// @description Chooses a random value from the array with specified probability distribution
/// @param choose_array The array of values to choose from. If empty array, the function will return the index instead.
/// @param array_probs The probability array of each values from the list. If empty, use uniform distribution.
/// @return The chosen element

function fnc_ChooseProb(_choose_array, _array_probs) {
	var _n_choose = array_length(_choose_array);
	var _n_probs = array_length(_array_probs);
	
	if (_n_probs == 0 && _n_choose == 0) {  // Error
		throw("Error on fnc_ChooseProb, empty arrays provided.");
	}
	else {
		if (_n_probs == 0) {	 // Set uniform distribution
			var _probs = array_create(_n_choose);
			var _k = 0;
			for (var _i=0; _i<_n_choose; _i++) {
				if (_i < _n_choose - 1) {
					_probs[_i] = 1/_n_choose;
					_k=_k+_probs[_i];
				}
				else {
					_probs[_i] = 1-_k;				
				}
			}
		}
		else {	// Use what has been given
			_probs = _array_probs;			
		}
	
		var _rnd = random(1);
		var _i = 0;
		var	_currProb = _probs[_i];
		var _cumProb = _currProb;
		while (_rnd > _cumProb) {
			_i++;
			var	_currProb = _probs[_i];
			var _cumProb = _cumProb + _currProb;
		}
		if (_n_choose == 0) {
			return _i;
		}
		else {		
			return _choose_array[_i];
		}
	}
}

/// @function fnc_ChooseProbList(choose_list, array_probs)
/// @description Chooses a random value from the list with specified probability distribution
/// @param choose_list The list of values to choose from
/// @param array_probs The probability array of each values from the list
/// @return The chosen element

function fnc_ChooseProbList(_choose_list, _array_probs) {
	var _rnd = random(1);
	var _i = 0;
	var	_currProb = _array_probs[_i];
	var _cumProb = _currProb;
	while (_rnd > _cumProb) {
		_i++;
		var	_currProb = _array_probs[_i];
		var _cumProb = _cumProb + _currProb;
	}
	return _choose_list[| _i];
}




/// @function fnc_KeyToString(_key)
/// @arg _key The keycode to name
/// @return The reeadable name of the keycode

function fnc_KeyToString(_key) {
	if (_key >= 48 && _key <= 90) { 
		return chr(_key);
	}
	else {
		switch(_key) {
		    case -1: return "Unassigned";
		    case vk_backspace: return "Backspace";
		    case vk_tab: return "Tab";
		    case vk_enter: return "Enter";
		    case vk_lshift: return "Left Shift";
		    case vk_lcontrol: return "Left Ctrl";
		    case vk_lalt: return "Left Alt";
			case vk_rshift: return "Right Shift";
		    case vk_rcontrol: return "Right Ctrl";
		    case vk_ralt: return "Right Alt";
			case vk_shift: return "Shift";
		    case vk_control: return "Ctrl";
		    case vk_alt: return "Alt";
			case vk_printscreen: return "Print Screen";
		    case vk_pause: return "Pause/Break";
		    case 20: return "Caps Lock";
		    case vk_escape: return "Esc";
			case vk_space: return "Space";
		    case vk_pageup: return "Page Up";
		    case vk_pagedown: return "Page Down";
		    case vk_end: return "End";
		    case vk_home: return "Home";
		    case vk_left: return "Left Arrow";
		    case vk_up: return "Up Arrow";
		    case vk_right: return "Right Arrow";
		    case vk_down: return "Down Arrow";
		    case vk_insert: return "Insert";
		    case vk_delete: return "Delete";
			case vk_divide: return "/";
		    case vk_numpad0: return "Numpad 0";
		    case vk_numpad1: return "Numpad 1";
		    case vk_numpad2: return "Numpad 2";
		    case vk_numpad3: return "Numpad 3";
		    case vk_numpad4: return "Numpad 4";
		    case vk_numpad5: return "Numpad 5";
		    case vk_numpad6: return "Numpad 6";
		    case vk_numpad7: return "Numpad 7";
		    case vk_numpad8: return "Numpad 8";
		    case vk_numpad9: return "Numpad 9";
		    case vk_multiply: return "Numpad *";
		    case vk_add: return "Numpad +";
			case vk_decimal: return "Numpad .";
		    case vk_subtract: return "Numpad -";    
		    case vk_f1: return "F1";
		    case vk_f2: return "F2";
		    case vk_f3: return "F3";
		    case vk_f4: return "F4";
		    case vk_f5: return "F5";
		    case vk_f6: return "F6";
		    case vk_f7: return "F7";
		    case vk_f8: return "F8";
		    case vk_f9: return "F9";
		    case vk_f10: return "F10";
		    case vk_f11: return "F11";
		    case vk_f12: return "F12";
		    case 144: return "Num Lock";
		    case 145: return "Scroll Lock";
		    case ord(";"): return ";";
		    case ord("="): return "=";
		    case ord("\\"): return "\\";    
		    case ord("["): return "[";
		    case ord("]"): return "]";
			default: return "other";
		}
	}
}

/// @function fnc_BackupDrawParams
/// @description Backup the draw parameters to variables

function fnc_BackupDrawParams() {
	tmpDrawFont = draw_get_font();
	tmpDrawHAlign = draw_get_halign();
	tmpDrawVAlign = draw_get_valign();
	tmpDrawColor = draw_get_color();
	tmpDrawAlpha = draw_get_alpha();	
}

/// @function fnc_RestoreDrawParams
/// @description Restore the draw parameters from variables

function fnc_RestoreDrawParams() {
	draw_set_font(tmpDrawFont);
	draw_set_halign(tmpDrawHAlign);
	draw_set_valign(tmpDrawVAlign);
	draw_set_color(tmpDrawColor);
	draw_set_alpha(tmpDrawAlpha);	
}


function fnc_BaseConvert(_string_number, _old_base, _new_base) {
	var number, oldbase, newbase, out;
	number = _string_number;
    //number = string_upper(_string_number);
    oldbase = _old_base;
    newbase = _new_base;
    out = "";
 
    var len, tab;
    len = string_length(number);
    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    var i, num;
    for (i=0; i<len; i+=1) {
        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
    }
 
    do {
        var divide, newlen;
        divide = 0;
        newlen = 0;
        for (i=0; i<len; i+=1) {
            divide = divide * oldbase + num[i];
            if (divide >= newbase) {
                num[newlen] = divide div newbase;
                newlen += 1;
                divide = divide mod newbase;
            } else if (newlen  > 0) {
                num[newlen] = 0;
                newlen += 1;
            }
        }
        len = newlen;
        out = string_char_at(tab, divide+1) + out;
    } until (len == 0);
 
    return out;
}

function in(_element, _array) {
	if (array_length(_array) == 0) {
		return false;
	}
	else {
		var _i=0;
		var _n = array_length(_array);
		var _found = false;
		while (_i<_n && !_found) {
			if (_array[_i] == _element) {
				_found = true;
			}
			else {
				_i++;
			}
		}
	}
	return _found;
}

#region String functions

/// @function		fnc_StringToList()
/// @description	Takes a string with separators and returns a DS list of the separated string
/// @param			string - Source string to split
/// @param			separator - Optional string separator to use. Default: comma
/// @param			remove leading and trailing spaces - Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
/// @return			A DS list with the string split into the parts
function fnc_StringToList() {
	if (argument_count == 0) {		
		throw ("String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	if (argument_count >= 2) {		
		var _separator = argument[1];
		if (string_length(_separator) != 1) {
			_separator = ",";
		}
	}
	else {
		var _separator = ",";
	}
	if (argument_count >= 3) {
		var _remove_lead_trail_spaces = argument[2];
	}
	else {
		var _remove_lead_trail_spaces = false;
	}
	
		
	// Process and split
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = fnc_String_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}


function fnc_String_cleanse() {
	var _str = argument[0];
	for (var _j=0; _j<32; _j++) {
		_str = string_replace_all(_str, chr(_j), "");
	}
	return _str;
}

function fnc_String_lrtrim(_string) {
	
	var _str = argument[0];
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;
	
}

#endregion


function Vec2(_x, _y) constructor {
	x = _x;
	y = _y;
}


function Vec3(_x, _y, _z) constructor {
	x = _x;
	y = _y;
	y = _z;
}

function display_get_gui_w() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) {
		return Game.adjusted_window_width;
	}
	else {
		return display_get_gui_width();
	}
}

function display_get_gui_h() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) {
		return Game.adjusted_window_height;
	}
	else {
		return display_get_gui_height();
	}
}



/// @function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel)
/// @description Perform easing with an animation curve with the equation: v = v_ini + lambda * (v_target-v_ini) where lambda changes from 0 to 1 according to the animation curve.
/// @param _v_ini initial value (when t=0).
/// @param _v_target target value (when t=1).
/// @param _t target value
/// @param _curve animation curve asset.
/// @param _curve_channel name of the curve channel.
/// @returns the interpolated value at _t.

function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel) {
	var _anim_channel = animcurve_get_channel(_curve, _curve_channel);
	var _lambda = animcurve_channel_evaluate(_anim_channel, _t);	
	return _v_ini + _lambda * (_v_target - _v_ini);
}


function array_find(_array, _element) {
	var _n = array_length(_array);
	var _found = false;
	var _i=0;
	while (_i<_n && !_found) {
		if (_array[_i] == _element) {
			_found = true;
		}
		else {
			_i++;
		}
	}
	if (_found) {
		return _i;
	}
	else {
		return -1;
	}
}


function array_resize_from_end(_array, _new_size) {
	var _n = array_length(_array);
	if (_new_size < _n) {		
		for (var _i=0; _i<_new_size; _i++) {
			_array[@_i] = _array[_i+_n-_new_size];
		}
		array_resize(_array, _new_size);
	}
}

function convert_gui_to_room(_vec2_gui) {
	// TODO						
}

function convert_room_to_gui(_vec2_room) {
	//if (ENABLE_LIVE and live_call()) return live_result;
	var _gw = display_get_gui_w();
	var _gh = display_get_gui_h();
	var _rw = room_width;
	var _rh = room_height;
			
	var _sw = surface_get_width(application_surface) * Game.application_surface_scaling;
	var _sh = surface_get_height(application_surface) * Game.application_surface_scaling;
	
	var _cw = camera_get_view_width(VIEW);
	var _ch = camera_get_view_height(VIEW);
	var _cx = camera_get_view_x(VIEW);
	var _cy = camera_get_view_y(VIEW);
	
	var _x_rel_cam = (_vec2_room.x - _cx)/_cw;
	var _y_rel_cam = (_vec2_room.y - _cy)/_ch;

	return new Vec2(_x_rel_cam * _sw + Game.gui_offset_x, _y_rel_cam * _sh + Game.gui_offset_y);

}


function device_mouse_dragswipe_register(_device) {
	Game.swipe_started_now[_device] = false;
	Game.swipe_ended_now[_device] = false;
	
	if (!Game.swiping[_device]) {
		Game.swiping[_device] = device_mouse_check_button_pressed(_device, mb_left);
		if (Game.swiping[_device]) { // Swipe started
			Game.swipe_start[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
			Game.swipe_end[_device] = -1;
			Game.swipe_started_now[_device] = true;
			Game.swipe_distance[_device] = -1;
			Game.swipe_time[_device] = get_timer();			
		}
	}
	else if (device_mouse_check_button_released(_device, mb_left)) { // Swipe ended
		Game.swiping[_device] = false;				
		Game.swipe_end[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
		Game.swipe_ended_now[_device] = true;
		Game.swipe_distance[_device] = point_distance(Game.swipe_start[_device].x, Game.swipe_start[_device].y, Game.swipe_end[_device].x, Game.swipe_end[_device].y);
		Game.swipe_start[_device] = -1;
		Game.swipe_time[_device] = (get_timer() - Game.swipe_time[_device])/1000000;		
	}
	else {
		// Swiping in progress
	}
}

function device_mouse_check_dragswipe_start(_device) {
	var _ret = Game.swipe_started_now[_device];
	if (_ret) show_debug_message("Device "+string(_device)+" Swiping started at "+string(Game.swipe_start[_device]));
	return _ret;	
}

function device_mouse_check_dragswipe_end(_device) {	
	var _ret = Game.swipe_ended_now[_device] && Game.swipe_distance[_device] > SWIPE_DETECTION_TOLERANCE;
	if (_ret) show_debug_message("Device "+string(_device)+" Swiping ended at "+string(Game.swipe_end[_device]));
	return _ret;
}

function device_mouse_check_dragswipe(_device) {
	var _ret = Game.swipe_start[_device] != -1 && Game.swipe_end[_device] != -1;
	if (_ret) show_debug_message("Device "+string(_device)+" Swiping in progress");
	return _ret;
}

function device_mouse_dragswipe_distance(_device) {
	var _ret = Game.swipe_distance[_device];
	show_debug_message("Device "+string(_device)+" distance swiped: "+string(_ret));
	return _ret;
}

function device_mouse_dragswipe_time(_device) {
	var _ret = Game.swipe_time[_device];
	show_debug_message("Device "+string(_device)+" time swiped: "+string(_ret));
	return _ret;	
}

function device_mouse_dragswipe_speed(_device) {
	var _ret = Game.swipe_distance[_device]/Game.swipe_time[_device];
	show_debug_message("Device "+string(_device)+" speed swiped: "+string(_ret));
	return _ret;
}


function gmcallback_fullscreen() {
	ToggleFullScreen();	
}

function array_print(_array) {
	var _n = array_length(_array);
	var _str = "[";
	for (var _i=0; _i<_n; _i++) {
		_str = _str+string(_array[_i]);
		if (_i<_n-1) _str = _str + " ";
	}
	_str = _str+"]";
	show_debug_message(_str);
}

function array_reverse(_array) {
	var _n = array_length(_array);
	var _new = [];
	for (var _i=_n-1; _i>=0; _i--) {
		array_push(_new, _array[_i]);
	}
	return _new;
}