// =============================================== Graphics =============================================== 
#macro VIEW view_camera[0]
#macro DISPLAY_WIDTH display_get_width()
#macro DISPLAY_HEIGHT display_get_height()
#macro ASPECT_RATIO_REAL DISPLAY_WIDTH/DISPLAY_HEIGHT
#macro ASPECT_RATIO_FORCED real(960/540)
#macro GUI_WIDTH display_get_gui_w()
#macro GUI_HEIGHT display_get_gui_h()
#macro ROOM_MOUSE_X device_mouse_x(0)
#macro ROOM_MOUSE_Y device_mouse_y(0)
#macro GUI_MOUSE_X device_mouse_x_to_gui(0)
#macro GUI_MOUSE_Y device_mouse_y_to_gui(0)

#macro SELECTED_ASPECT_RATIO ASPECT_RATIO_FORCED

enum SCALING_TYPE {	
	RESOLUTION_ADAPTED_TO_WINDOW,				// "normal" mode - set resolution AND window size and let resolution scale to window size
	RESOLUTION_INDEPENDENT_OF_WINDOW,			// Set resolution AND set window size independently
	RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED	// Resolution and window independent, but resolution scaled to max possible size within window. If aspect ratios match, it will be the same as RESOLUTION_ADAPTED_TO_WINDOW, but if different aspect ratio, will work (i.e. square resolution on rectangular window)
}

#macro SELECTED_SCALING SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED
#macro RESOLUTION_MAXIMIZED_SCALING_FACTOR -1	// If scaling = RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED: if this macro var is -1 it will maximize to window/diplay; if it's a whole number, it will instead multiply the app surface by that scaling factor.
#macro BASE_RESOLUTION_W 320
#macro BASE_RESOLUTION_H 320
#macro BASE_WINDOW_SIZE_W 960
#macro BASE_WINDOW_SIZE_H 540

// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// 1 - Base resolution: this is the game resolution it's designed for (this will be the camera view size)
//		Notes: to achieve a perfect scaling for a 16:9 aspect ratio, one can take these values:
//		1x	1920x1080 
//		2x	960x540
//		3x	640x360
//		4x	480x270
//		5x	384x216
//		6x	320x180
//		Non perfect scaling: 1280x720
// 2 - Window resolution: OPTIONAL. Set this only if you want the WINDOW to be different size than the actual GAME RESOLUTION
// Applies to WINDOW_INDEPENDENT_OF_RESOLUTION (in this case there will be no scaling) and to RESOLUTION_SCALED_TO_WINDOW (in this case the resolution will scale)
// For RESOLUTION_SCALED_TO_WINDOW, for landscape aspect ratios, it will adjust the width automatically (so choose height wisely); for portrait aspect ratios, it will adjust the height automatically (so choose width wisely)

#macro CENTER_WINDOW true


#macro MOBILE_DEVICE ((os_type == os_android || os_type == os_ios) && os_browser == browser_not_a_browser)
#macro BROWSER (os_browser != browser_not_a_browser)


// =============================================== Technical =============================================== 

#macro TILE_SIZE 16
#macro GRID_RESOLUTION 16
#macro GAMEPAD_THRESHOLD 0.3

#macro ENABLE_SCOREBOARD false
#macro SCOREBOARD_SALT "b1yEctiv0"

enum TRANSITION {
	FADE_OUT,
	STRIPES_HORIZONTAL,
	STRIPES_VERTICAL,
	SQUARES
}

enum FACING {
	EAST,
	WEST,
	NORTH,
	SOUTH
}


#macro SCREENSHAKE_STRENGTH 4
#macro SCREENSHAKE_DURATION 10

// ICONS


#macro ICON_MUSIC_OFF		chr(base_convert("E440", 16, 10))
#macro ICON_MUSIC_ON		chr(base_convert("E405", 16, 10))
#macro ICON_SOUND_ON		chr(base_convert("E050", 16, 10))
#macro ICON_SOUND_OFF		chr(base_convert("E04F", 16, 10))
#macro ICON_RETRY			chr(base_convert("E042", 16, 10))
#macro ICON_PAUSE			chr(base_convert("E034", 16, 10))
#macro ICON_PALETTE			chr(base_convert("E40A", 16, 10))
#macro ICON_CELL_NO_SIGNAL	chr(base_convert("F0AC", 16, 10))
#macro ICON_CELL_SIGNAL		chr(base_convert("E202", 16, 10))
#macro ICON_BATTERY			chr(base_convert("E1A4", 16, 10))
#macro ICON_MAIL			chr(base_convert("E0BE", 16, 10))
#macro ICON_MAIL_READ		chr(base_convert("E151", 16, 10))
#macro ICON_BACK			chr(base_convert("E5C4", 16, 10))
#macro ICON_TIMER			chr(base_convert("E425", 16, 10))
#macro ICON_ATTACHMENT		chr(base_convert("E226", 16, 10))
#macro ICON_HOME			chr(base_convert("E88A", 16, 10))
#macro ICON_DOWN			chr(base_convert("E5DB", 16, 10))
#macro ICON_UP				chr(base_convert("E5D8", 16, 10))
#macro ICON_HELP			chr(base_convert("F1C0", 16, 10))
#macro ICON_SETTINGS		chr(base_convert("E8B8", 16, 10))
#macro ICON_CHECKBOX_ON 	chr(base_convert("E834", 16, 10))
#macro ICON_CHECKBOX_OFF	chr(base_convert("E835", 16, 10))
#macro ICON_CLOSE			chr(base_convert("E5CD", 16, 10))


// SWIPE DETECTION TOLERANCE - increase to only recognize swipes of certain distance. For drag, use 0
#macro SWIPE_DETECTION_TOLERANCE 0

#macro ENABLE_LIVE false

#macro SCALE Game.application_surface_scaling