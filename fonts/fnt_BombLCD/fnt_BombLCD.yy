{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "LCD",
  "styleName": "Bold",
  "size": 96.0,
  "bold": true,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 4,
  "glyphs": {
    "32": {"x":2,"y":2,"w":38,"h":211,"character":32,"shift":38,"offset":0,},
    "33": {"x":263,"y":428,"w":27,"h":211,"character":33,"shift":21,"offset":-3,},
    "34": {"x":228,"y":428,"w":33,"h":211,"character":34,"shift":41,"offset":4,},
    "35": {"x":154,"y":428,"w":72,"h":211,"character":35,"shift":77,"offset":0,},
    "36": {"x":81,"y":428,"w":71,"h":211,"character":36,"shift":67,"offset":-2,},
    "37": {"x":2,"y":428,"w":77,"h":211,"character":37,"shift":77,"offset":0,},
    "38": {"x":1904,"y":215,"w":74,"h":211,"character":38,"shift":77,"offset":0,},
    "39": {"x":1886,"y":215,"w":16,"h":211,"character":39,"shift":21,"offset":2,},
    "40": {"x":1846,"y":215,"w":38,"h":211,"character":40,"shift":33,"offset":-2,},
    "41": {"x":1806,"y":215,"w":38,"h":211,"character":41,"shift":33,"offset":-3,},
    "42": {"x":292,"y":428,"w":28,"h":211,"character":42,"shift":32,"offset":2,},
    "43": {"x":1756,"y":215,"w":48,"h":211,"character":43,"shift":54,"offset":3,},
    "44": {"x":1664,"y":215,"w":16,"h":211,"character":44,"shift":21,"offset":2,},
    "45": {"x":1620,"y":215,"w":42,"h":211,"character":45,"shift":77,"offset":1,},
    "46": {"x":1603,"y":215,"w":15,"h":211,"character":46,"shift":21,"offset":3,},
    "47": {"x":1529,"y":215,"w":72,"h":211,"character":47,"shift":64,"offset":-4,},
    "48": {"x":1456,"y":215,"w":71,"h":211,"character":48,"shift":67,"offset":-2,},
    "49": {"x":1423,"y":215,"w":31,"h":211,"character":49,"shift":67,"offset":23,},
    "50": {"x":1349,"y":215,"w":72,"h":211,"character":50,"shift":67,"offset":-3,},
    "51": {"x":1279,"y":215,"w":68,"h":211,"character":51,"shift":67,"offset":-1,},
    "52": {"x":1212,"y":215,"w":65,"h":211,"character":52,"shift":67,"offset":5,},
    "53": {"x":1682,"y":215,"w":72,"h":211,"character":53,"shift":67,"offset":-2,},
    "54": {"x":322,"y":428,"w":64,"h":211,"character":54,"shift":67,"offset":-2,},
    "55": {"x":388,"y":428,"w":64,"h":211,"character":55,"shift":67,"offset":5,},
    "56": {"x":454,"y":428,"w":71,"h":211,"character":56,"shift":67,"offset":-2,},
    "57": {"x":1727,"y":428,"w":64,"h":211,"character":57,"shift":67,"offset":5,},
    "58": {"x":1706,"y":428,"w":19,"h":211,"character":58,"shift":21,"offset":1,},
    "59": {"x":1682,"y":428,"w":22,"h":211,"character":59,"shift":21,"offset":-1,},
    "60": {"x":1655,"y":428,"w":25,"h":211,"character":60,"shift":25,"offset":2,},
    "61": {"x":1602,"y":428,"w":51,"h":211,"character":61,"shift":52,"offset":0,},
    "62": {"x":1575,"y":428,"w":25,"h":211,"character":62,"shift":25,"offset":-2,},
    "63": {"x":1522,"y":428,"w":51,"h":211,"character":63,"shift":59,"offset":10,},
    "64": {"x":1446,"y":428,"w":74,"h":211,"character":64,"shift":77,"offset":0,},
    "65": {"x":1372,"y":428,"w":72,"h":211,"character":65,"shift":67,"offset":-3,},
    "66": {"x":1297,"y":428,"w":73,"h":211,"character":66,"shift":67,"offset":-4,},
    "67": {"x":1225,"y":428,"w":70,"h":211,"character":67,"shift":67,"offset":-2,},
    "68": {"x":1150,"y":428,"w":73,"h":211,"character":68,"shift":67,"offset":-4,},
    "69": {"x":1084,"y":428,"w":64,"h":211,"character":69,"shift":59,"offset":-2,},
    "70": {"x":1017,"y":428,"w":65,"h":211,"character":70,"shift":59,"offset":-3,},
    "71": {"x":949,"y":428,"w":66,"h":211,"character":71,"shift":67,"offset":-2,},
    "72": {"x":875,"y":428,"w":72,"h":211,"character":72,"shift":67,"offset":-3,},
    "73": {"x":846,"y":428,"w":27,"h":211,"character":73,"shift":21,"offset":-3,},
    "74": {"x":773,"y":428,"w":71,"h":211,"character":74,"shift":67,"offset":-2,},
    "75": {"x":699,"y":428,"w":72,"h":211,"character":75,"shift":65,"offset":-3,},
    "76": {"x":646,"y":428,"w":51,"h":211,"character":76,"shift":59,"offset":-2,},
    "77": {"x":527,"y":428,"w":117,"h":211,"character":77,"shift":112,"offset":-3,},
    "78": {"x":1138,"y":215,"w":72,"h":211,"character":78,"shift":67,"offset":-3,},
    "79": {"x":1066,"y":215,"w":70,"h":211,"character":79,"shift":67,"offset":-2,},
    "80": {"x":992,"y":215,"w":72,"h":211,"character":80,"shift":67,"offset":-3,},
    "81": {"x":1437,"y":2,"w":70,"h":211,"character":81,"shift":67,"offset":-2,},
    "82": {"x":1314,"y":2,"w":72,"h":211,"character":82,"shift":67,"offset":-3,},
    "83": {"x":1241,"y":2,"w":71,"h":211,"character":83,"shift":67,"offset":-2,},
    "84": {"x":1182,"y":2,"w":57,"h":211,"character":84,"shift":64,"offset":10,},
    "85": {"x":1109,"y":2,"w":71,"h":211,"character":85,"shift":67,"offset":-2,},
    "86": {"x":1039,"y":2,"w":68,"h":211,"character":86,"shift":67,"offset":2,},
    "87": {"x":920,"y":2,"w":117,"h":211,"character":87,"shift":112,"offset":-2,},
    "88": {"x":846,"y":2,"w":72,"h":211,"character":88,"shift":64,"offset":-4,},
    "89": {"x":773,"y":2,"w":71,"h":211,"character":89,"shift":67,"offset":-2,},
    "90": {"x":708,"y":2,"w":63,"h":211,"character":90,"shift":57,"offset":-3,},
    "91": {"x":1388,"y":2,"w":47,"h":211,"character":91,"shift":39,"offset":-4,},
    "92": {"x":660,"y":2,"w":46,"h":211,"character":92,"shift":64,"offset":9,},
    "93": {"x":544,"y":2,"w":47,"h":211,"character":93,"shift":39,"offset":-4,},
    "94": {"x":478,"y":2,"w":64,"h":211,"character":94,"shift":77,"offset":0,},
    "95": {"x":431,"y":2,"w":45,"h":211,"character":95,"shift":52,"offset":3,},
    "96": {"x":404,"y":2,"w":25,"h":211,"character":96,"shift":77,"offset":2,},
    "97": {"x":330,"y":2,"w":72,"h":211,"character":97,"shift":67,"offset":-3,},
    "98": {"x":255,"y":2,"w":73,"h":211,"character":98,"shift":67,"offset":-4,},
    "99": {"x":183,"y":2,"w":70,"h":211,"character":99,"shift":67,"offset":-2,},
    "100": {"x":108,"y":2,"w":73,"h":211,"character":100,"shift":67,"offset":-4,},
    "101": {"x":42,"y":2,"w":64,"h":211,"character":101,"shift":59,"offset":-2,},
    "102": {"x":593,"y":2,"w":65,"h":211,"character":102,"shift":59,"offset":-3,},
    "103": {"x":1509,"y":2,"w":66,"h":211,"character":103,"shift":67,"offset":-2,},
    "104": {"x":145,"y":215,"w":72,"h":211,"character":104,"shift":67,"offset":-3,},
    "105": {"x":1577,"y":2,"w":27,"h":211,"character":105,"shift":21,"offset":-3,},
    "106": {"x":845,"y":215,"w":72,"h":211,"character":106,"shift":67,"offset":-2,},
    "107": {"x":771,"y":215,"w":72,"h":211,"character":107,"shift":65,"offset":-3,},
    "108": {"x":718,"y":215,"w":51,"h":211,"character":108,"shift":59,"offset":-2,},
    "109": {"x":644,"y":215,"w":72,"h":211,"character":109,"shift":67,"offset":-3,},
    "110": {"x":570,"y":215,"w":72,"h":211,"character":110,"shift":67,"offset":-3,},
    "111": {"x":498,"y":215,"w":70,"h":211,"character":111,"shift":67,"offset":-2,},
    "112": {"x":424,"y":215,"w":72,"h":211,"character":112,"shift":67,"offset":-3,},
    "113": {"x":352,"y":215,"w":70,"h":211,"character":113,"shift":67,"offset":-2,},
    "114": {"x":278,"y":215,"w":72,"h":211,"character":114,"shift":67,"offset":-3,},
    "115": {"x":919,"y":215,"w":71,"h":211,"character":115,"shift":67,"offset":-2,},
    "116": {"x":219,"y":215,"w":57,"h":211,"character":116,"shift":64,"offset":10,},
    "117": {"x":72,"y":215,"w":71,"h":211,"character":117,"shift":67,"offset":-2,},
    "118": {"x":2,"y":215,"w":68,"h":211,"character":118,"shift":67,"offset":2,},
    "119": {"x":1942,"y":2,"w":72,"h":211,"character":119,"shift":67,"offset":-3,},
    "120": {"x":1868,"y":2,"w":72,"h":211,"character":120,"shift":64,"offset":-4,},
    "121": {"x":1804,"y":2,"w":62,"h":211,"character":121,"shift":67,"offset":9,},
    "122": {"x":1739,"y":2,"w":63,"h":211,"character":122,"shift":57,"offset":-3,},
    "123": {"x":1688,"y":2,"w":49,"h":211,"character":123,"shift":77,"offset":1,},
    "124": {"x":1657,"y":2,"w":29,"h":211,"character":124,"shift":21,"offset":-4,},
    "125": {"x":1606,"y":2,"w":49,"h":211,"character":125,"shift":77,"offset":1,},
    "126": {"x":1793,"y":428,"w":59,"h":211,"character":126,"shift":77,"offset":0,},
    "9647": {"x":1854,"y":428,"w":74,"h":211,"character":9647,"shift":124,"offset":25,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_BombLCD",
  "tags": [],
  "resourceType": "GMFont",
}